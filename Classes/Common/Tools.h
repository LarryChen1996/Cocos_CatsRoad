#ifndef __TOOLS_H__
#define __TOOLS_H__

#include "cocos2d.h"
#include "Common/View/Button.h"

typedef std::function<void(cocos2d::Ref*)> buttonCallback;

class Tools{
    public:
        // class const variable
        const static int VECTOR_NON_INDEX = 1;

        // function
        template <typename T>
        static T randomNumber(T min, T max);

        template <typename VectorValue>
        static int findVectorIndex(const std::vector<VectorValue>& targetVector, VectorValue targetValue);

        template <typename MapKey, typename MapValue>
        static MapValue getMapRandomValue(const std::map<MapKey, MapValue>& targetMap);

        template <typename VectorValue>
        static VectorValue getVectorRandomValue(const std::vector<VectorValue>& targetVector);

        template <typename VectorValue>
        static std::vector<VectorValue> getVectorsDiff(const std::vector<VectorValue>& v1, const std::vector<VectorValue>& v2);

        static std::string formatString(std::string inputString, int number);

        static cocos2d::Vector<cocos2d::SpriteFrame*> getAnimationFrames(
            std::string fileName,
            int startNumber,
            int endNumber
        );
};

template <typename T>
T Tools::randomNumber(T min, T max){
    return static_cast<T>((rand() % static_cast<int>(max - min + 1)) + static_cast<int>(min));
};

template <typename VectorValue>
int Tools::findVectorIndex(const std::vector<VectorValue>& targetVector, VectorValue targetValue){
    auto iter = std::find(targetVector.begin(), targetVector.end(), targetValue);
    if (iter != targetVector.end()){
        return std::distance(targetVector.begin(), iter);
    };
    return Tools::VECTOR_NON_INDEX;
};

template <typename MapKey, typename MapValue>
MapValue Tools::getMapRandomValue(const std::map<MapKey, MapValue>& targetMap){
    auto iter = targetMap.begin();
    std::advance(iter, rand() % targetMap.size());
    return iter->second;
};

template <typename VectorValue>
VectorValue Tools::getVectorRandomValue(const std::vector<VectorValue>& targetVector){
    auto iter = targetVector.begin();
    std::advance(iter, rand() % targetVector.size());
    return *iter;
};

template <typename VectorValue>
std::vector<VectorValue> Tools::getVectorsDiff(const std::vector<VectorValue>& v1, const std::vector<VectorValue>& v2){
    std::vector<VectorValue> diffVector;
    std::set_difference(
        v1.begin(),
        v1.end(),
        v2.begin(),
        v2.end(),
        std::inserter(diffVector, diffVector.begin())
    );
    return diffVector;
};

#endif // __TOOLS_H__