#include "Common/Tools.h"
#include "stdlib.h"
#include "Common/View/Button.h"

USING_NS_CC;

std::string Tools::formatString(std::string inputString, int number){
    char buff[255];
    snprintf(buff, sizeof(buff), inputString.c_str(), number);
    std::string buffAsStdStr(buff);
    return buffAsStdStr;
};

Vector<SpriteFrame*> Tools::getAnimationFrames(std::string fileName, int startNumber, int endNumber){
    auto spriteFrameCache = SpriteFrameCache::getInstance();
    Vector<SpriteFrame*> animFrames(endNumber - startNumber + 1);
    
    for (int index = startNumber; index <= endNumber; index++){
        SpriteFrame* frame = spriteFrameCache->getSpriteFrameByName(Tools::formatString(fileName, index));
        animFrames.pushBack(frame);
    };
    return animFrames;
};