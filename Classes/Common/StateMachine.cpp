#include "Common/StateMachine.h"

StateMachine* StateMachine::create(StateMachineState initState, void* scope){
    StateMachine* instance = new(std::nothrow) StateMachine(initState, scope);
    return instance;
};

StateMachine::StateMachine(StateMachineState initState, void* scope){
    _scope = scope;
    _currentState = initState;
    _nextState = "";
    _isFirstExec = false;
    _stateFunc = {};
};

void StateMachine::addState(StateMachineState state, std::function<void(bool, float)> stateFunc){
    auto iter = _stateFunc.find(state);
    if (iter != _stateFunc.end()){
        CCLOG("[StateMachine] addState, %s 已存在", state.c_str());
        return;
    };

    _stateFunc.insert(std::pair<StateMachineState, std::function<void(bool, float)>>(state, stateFunc));
};

void StateMachine::transfer(StateMachineState nextState){
    _nextState = nextState;
};

StateMachineState StateMachine::getCurrentState(){
    return _currentState;
};

bool StateMachine::isState(StateMachineState state){
    return state == _currentState;
};

void StateMachine::update(float dt){
    if (_nextState != ""){
        _currentState = _nextState;
        _nextState = "";
        _isFirstExec = true;
    };

    auto iter = _stateFunc.find(_currentState);
    if (iter == _stateFunc.end()){
        return;
    };

    auto func = iter->second;
    func(_isFirstExec, dt);
    _isFirstExec = false;
};