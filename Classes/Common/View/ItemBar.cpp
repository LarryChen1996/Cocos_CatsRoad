#include "Common/View/ItemBar.h"

USING_NS_CC;

const Vec2 ItemBar::DEFAULT_ITEM_POS_OFFSET = Vec2(75, 0);

/* ---------------------------------------------  Creator  --------------------------------------------- */

ItemBar* ItemBar::create(int maxItemAmount, std::string itemFilePath, std::string itemEmptyFilePath){
    ItemBar* instance = new(std::nothrow) ItemBar(maxItemAmount, itemFilePath, itemEmptyFilePath);
    
    if (instance){ 
        instance->autorelease();
        return instance;
    }else{
        delete instance;
        instance = nullptr;
        return nullptr;
    };
};

ItemBar::ItemBar(int maxItemAmount, std::string itemFilePath, std::string itemEmptyFilePath){
    _maxItemAmount = maxItemAmount;

    _itemsEmptyVector = this->_createItemsEmpty(maxItemAmount, itemEmptyFilePath);
    _itemsVector = this->_createItems(maxItemAmount, itemFilePath);
    this->setItemOffset(DEFAULT_ITEM_POS_OFFSET);
};

/* ---------------------------------------------  Public  --------------------------------------------- */

void ItemBar::setItemOffset(Vec2 itemOffset){
    for (int itemIndex = 0; itemIndex < _maxItemAmount; itemIndex++){
        Vec2 pos = Vec2(itemOffset.x * itemIndex, itemOffset.y * itemIndex);
        _itemsEmptyVector.at(itemIndex)->setPosition(pos);
        _itemsVector.at(itemIndex)->setPosition(pos);
    };
};

void ItemBar::setItemAmount(int itemAmount){
    for (int itemIndex = 0; itemIndex < _maxItemAmount; itemIndex++){
        bool isItemVisible = itemIndex < itemAmount;
        _itemsVector.at(itemIndex)->setVisible(isItemVisible);
    };
};

/* ---------------------------------------------  Private  --------------------------------------------- */

std::vector<Sprite*> ItemBar::_createItemsEmpty(int maxItemAmount, std::string itemEmptyFilePath){
    std::vector<Sprite*> itemsEmptyVector;

    for (int itemIndex = 0; itemIndex < maxItemAmount; itemIndex++){
        Sprite* itemEmpty = Sprite::create(itemEmptyFilePath);
        this->addChild(itemEmpty);
        itemsEmptyVector.push_back(itemEmpty);
    };
    return itemsEmptyVector;
};

std::vector<Sprite*> ItemBar::_createItems(int maxItemAmount, std::string itemFilePath){
    std::vector<Sprite*> itemsVector;

    for (int itemIndex = 0; itemIndex < maxItemAmount; itemIndex++){
        Sprite* item = Sprite::create(itemFilePath);
        this->addChild(item);
        itemsVector.push_back(item);
    };
    return itemsVector;
};