#ifndef __AUDIO_BUTTON_H__
#define __AUDIO_BUTTON_H__

#include "cocos2d.h"
#include "Common/View/SwitchButton.h"
#include "editor-support/cocostudio/SimpleAudioEngine.h"

class AudioButton : public cocos2d::Node{
    public:
        static std::string switchOnFilePath;
        static std::string switchOffFilePath;

        // creator
        static AudioButton* create();
        AudioButton();

        // function
        void setEnalbed(bool enabled);
    
    private: 
        // class const variable
        const static float MAX_VOLUME;
        const static float MIN_VOLUME;

        // variable
        CocosDenshion::SimpleAudioEngine* _audioEngine;
        SwitchButton* _audioButton;

        // funciton
        SwitchButton* _createAudioButton();
};

#endif // __AUDIO_BUTTON_H__