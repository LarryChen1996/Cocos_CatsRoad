#include "Common/View/AudioButton.h"
#include "Common/View/SwitchButton.h"
#include "editor-support/cocostudio/SimpleAudioEngine.h"

/* ---------------- Public Const ---------------- */
std::string AudioButton::switchOnFilePath = "";
std::string AudioButton::switchOffFilePath = "";

/* ---------------- Private Const ---------------- */
const float AudioButton::MAX_VOLUME = 1.0;
const float AudioButton::MIN_VOLUME = 0.0;

/* ---------------------------------------------  Creator  --------------------------------------------- */

AudioButton* AudioButton::create(){
    AudioButton* instance = new(std::nothrow) AudioButton();
    
    if (instance){ 
        instance->autorelease();
        return instance;
    }else{
        delete instance;
        instance = nullptr;
        return nullptr;
    };
};

AudioButton::AudioButton(){
    _audioEngine = CocosDenshion::SimpleAudioEngine::getInstance();
    _audioButton = this->_createAudioButton();
};

/* ---------------------------------------------  Public  --------------------------------------------- */

void AudioButton::setEnalbed(bool enabled){
    _audioButton->setEnabled(enabled);
};

/* ---------------------------------------------  Private  --------------------------------------------- */

SwitchButton* AudioButton::_createAudioButton(){
    SwitchButton* audioButton = SwitchButton::create(
        AudioButton::switchOnFilePath,
        AudioButton::switchOffFilePath,
        _audioEngine->getEffectsVolume() == AudioButton::MAX_VOLUME
    );
    audioButton->setClickSwitchOnButtonCallback(
        [this](){
            _audioButton->setSwitchOn(false);
            _audioEngine->setEffectsVolume(AudioButton::MIN_VOLUME);
            _audioEngine->setBackgroundMusicVolume(AudioButton::MIN_VOLUME);
        }
    );
    audioButton->setClickSwitchOffButtonCallback(
        [this](){
            _audioButton->setSwitchOn(true);
            _audioEngine->setEffectsVolume(AudioButton::MAX_VOLUME);
            _audioEngine->setBackgroundMusicVolume(AudioButton::MAX_VOLUME);
        }
    );
    this->addChild(audioButton);
    return audioButton;
};