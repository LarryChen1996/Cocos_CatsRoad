#ifndef __BUTTON_H__
#define __BUTTON_H__

#include "cocos2d.h"

class Button : public cocos2d::Menu{
    public:
        // creator
        static Button* create(
                                const std::string& normalFilePath,
                                const std::string& pressedFilePath,
                                const cocos2d::ccMenuCallback& callback
                        );
        Button(cocos2d::MenuItem* item);

        // function
        void setFixedPriority(int fixedPriotiy);

    protected:
        cocos2d::EventListener* _touchListener;
};

#endif // __BUTTON_H__