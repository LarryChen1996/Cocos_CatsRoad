#include "Common/View/Button.h"

USING_NS_CC;

Button* Button::create(
    const std::string& normalFilePath,
    const std::string& pressedFilePath,
    const cocos2d::ccMenuCallback& callback
){
    MenuItemImage* menuItemImage = MenuItemImage::create(normalFilePath, pressedFilePath, callback);
    Button* instance = new(std::nothrow) Button(menuItemImage);
    
    if (instance){ 
        instance->autorelease();
        return instance;
    }else{
        delete instance;
        instance = nullptr;
        return nullptr;
    };
};

Button::Button(MenuItem* item){
    // init
    Vector<MenuItem*> items;
    items.pushBack(item);
    this->initWithArray(items);

    // modify touch event listener
    _eventDispatcher->removeEventListenersForTarget(this, true);
    auto touchListener = EventListenerTouchOneByOne::create();
    touchListener->setSwallowTouches(true);
    touchListener->onTouchBegan = CC_CALLBACK_2(Menu::onTouchBegan, this);
    touchListener->onTouchMoved = CC_CALLBACK_2(Menu::onTouchMoved, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(Menu::onTouchEnded, this);
    touchListener->onTouchCancelled = CC_CALLBACK_2(Menu::onTouchCancelled, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
    _touchListener = touchListener;
};

// TODO: 無用
void Button::setFixedPriority(int fixedPriority){
    // _eventDispatcher->setPriority(_touchListener, fixedPriority);
};