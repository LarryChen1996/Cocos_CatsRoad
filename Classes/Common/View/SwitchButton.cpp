#include "Common/View/SwitchButton.h"

USING_NS_CC;

/* ---------------------------------------------  Creator  --------------------------------------------- */

SwitchButton* SwitchButton::create(
    std::string switchOnFilePath,
    std::string switchOffFilePath,
    bool isSwitchOn
){
    SwitchButton* instance = new(std::nothrow) SwitchButton(switchOnFilePath, switchOffFilePath, isSwitchOn);
    
    if (instance){ 
        instance->autorelease();
        return instance;
    }else{
        delete instance;
        instance = nullptr;
        return nullptr;
    };
};

SwitchButton::SwitchButton(
    std::string switchOnFilePath,
    std::string switchOffFilePath,
    bool isSwitchOn
){
    _switchOnButton  = this->_createSwitchOnButton(switchOnFilePath);
    _switchOffButton = this->_createSwitchOffButton(switchOffFilePath);
    this->setSwitchOn(isSwitchOn);
};

/* ---------------------------------------------  Public  --------------------------------------------- */

void SwitchButton::setClickSwitchOnButtonCallback(std::function<void()> callback){
    _clickSwitchOnCallback = callback;
};

void SwitchButton::setClickSwitchOffButtonCallback(std::function<void()> callback){
    _clickSwitchOffCallback = callback;
};

void SwitchButton::setEnabled(bool enabled){
    _switchOnButton->setEnabled(enabled);
    _switchOffButton->setEnabled(enabled);
};

void SwitchButton::setSwitchOn(bool isSwitchOn){
    _switchOnButton->setVisible(isSwitchOn);
    _switchOnButton->setEnabled(isSwitchOn);
    _switchOffButton->setVisible(!isSwitchOn);
    _switchOffButton->setEnabled(!isSwitchOn);
};

/* ---------------------------------------------  Private  --------------------------------------------- */

Button* SwitchButton::_createSwitchOnButton(std::string filePath){
    Button* switchOnButton = Button::create(
        filePath,
        filePath, 
        [this](Ref* sender){
            if (_clickSwitchOnCallback != nullptr){
                _clickSwitchOnCallback();
            }
        }
    );
    switchOnButton->setPosition(Vec2(0, 0));
    this->addChild(switchOnButton);
    return switchOnButton;
};

Button* SwitchButton::_createSwitchOffButton(std::string filePath){
    Button* switchOffButton = Button::create(
        filePath,
        filePath, 
        [this](Ref* sender){
            if (_clickSwitchOffCallback != nullptr){
                _clickSwitchOffCallback();
            }
        }
    );
    switchOffButton->setPosition(Vec2(0, 0));
    this->addChild(switchOffButton);
    return switchOffButton;
};