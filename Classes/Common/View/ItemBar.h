#ifndef __ITEMBAR_H__
#define __ITEMBAR_H__

#include "cocos2d.h"

class ItemBar : public cocos2d::Node{
    public:
        // creator
        static ItemBar* create(int maxItemAmount, std::string itemFilePath, std::string itemEmptyFilePath);
        ItemBar(int maxItemAmount, std::string itemFilePath, std::string itemEmptyFilePath);

        // function
        void setItemOffset(cocos2d::Vec2 offset);
        void setItemAmount(int itemAmount);

    private: 
        // class const variable
        const static cocos2d::Vec2 DEFAULT_ITEM_POS_OFFSET;

        // variable
        int _maxItemAmount;
        std::vector<cocos2d::Sprite*> _itemsEmptyVector;
        std::vector<cocos2d::Sprite*> _itemsVector;

        // function 
        std::vector<cocos2d::Sprite*> _createItemsEmpty(int maxItemAmount, std::string itemEmptyFilePath);
        std::vector<cocos2d::Sprite*> _createItems(int maxItemAmount, std::string itemFilePath);
};

#endif // __ITEMBAR_H__