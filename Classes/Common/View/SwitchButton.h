#ifndef __SWITCH_BUTTON_H__
#define __SWITCH_BUTTON_H__

#include "cocos2d.h"
#include "Common/View/Button.h"

class SwitchButton : public cocos2d::Node{
    public:
        // creator
        static SwitchButton* create(
            std::string switchOnFilePath,
            std::string switchOffFilePath,
            bool isSwitchOn
        );
        SwitchButton(
            std::string switchOnFilePath,
            std::string switchOffFilePath,
            bool isSwitchOn
        );

        // function
        void setClickSwitchOnButtonCallback(std::function<void()> callback);
        void setClickSwitchOffButtonCallback(std::function<void()> callback);
        void setEnabled(bool enabled);
        void setSwitchOn(bool isSwitchOn);
    
    private: 
        // variable
        Button* _switchOnButton;
        Button* _switchOffButton;
        std::function<void()> _clickSwitchOnCallback;
        std::function<void()> _clickSwitchOffCallback;

        // function
        Button* _createSwitchOnButton(std::string filePath);
        Button* _createSwitchOffButton(std::string filePath);
};

#endif // __SWITCH_BUTTON_H__