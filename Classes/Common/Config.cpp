#include "Config.h"

USING_NS_CC;

const Size Config::WIN_SIZE = Size(1136, 640);
const int  Config::CENTER_X = Config::WIN_SIZE.width / 2;
const int  Config::CENTER_Y = Config::WIN_SIZE.height / 2;
const Vec2 Config::CENTER   = Vec2(Config::CENTER_X, Config::CENTER_Y);

const std::string Config::GAME_BMFONT_FILA_PATH = "font/cats_road_font.fnt";

const std::string Config::LOBBY_NAME = "LOBBY_LAYER";
const std::string Config::GAME_NAME  = "GAME_LAYER";

