#ifndef __CONFIG_H__
#define __CONFIG_H__

#include "cocos2d.h"

class Config{  
    public:
        const static cocos2d::Size WIN_SIZE;
        const static int CENTER_X;
        const static int CENTER_Y;
        const static cocos2d::Vec2 CENTER;
    
        const static std::string GAME_BMFONT_FILA_PATH;

        const static std::string LOBBY_NAME;
        const static std::string GAME_NAME;
};

#endif // __CONFIG_H__