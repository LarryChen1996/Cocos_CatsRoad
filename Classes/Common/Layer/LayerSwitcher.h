#ifndef __LAYERSWITCHER_H__
#define __LAYERSWITCHER_H__

#include "cocos2d.h"
#include "BaseLayer.h"

class LayerSwitcher{  
    public:
        // creator
        static LayerSwitcher* getInstance();
        LayerSwitcher();
    
        // function
        void registerTargetScene(cocos2d::Scene* targetScene);
        void addLayer(std::string layerName, BaseLayer* layer);
        void switchLayer(std::string layerName);

    private:
        // variable
        std::map<std::string, BaseLayer*> _layerMaps;
        BaseLayer* _currentLayer;
        cocos2d::Scene* _targetScene;
};

#endif // __LAYERSWITCHER_H__