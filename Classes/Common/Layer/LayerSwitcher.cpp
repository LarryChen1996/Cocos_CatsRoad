#include "LayerSwitcher.h"
#include "BaseLayer.h"

USING_NS_CC;

static LayerSwitcher* _layerSwitcherInstance = nullptr;

LayerSwitcher* LayerSwitcher::getInstance(){
    if (!_layerSwitcherInstance){
        _layerSwitcherInstance = new (std::nothrow) LayerSwitcher();
    };
    return _layerSwitcherInstance;
};

LayerSwitcher::LayerSwitcher(){
    _currentLayer = nullptr;
};

void LayerSwitcher::registerTargetScene(Scene* targetScene){
    _targetScene = targetScene;
};

void LayerSwitcher::addLayer(std::string layerName, BaseLayer* layer){
    auto iter = _layerMaps.find(layerName);
    if (iter != _layerMaps.end()){
        CCLOG("[LayerSwitcher] addLayer, %s 已存在", layerName.c_str());
        return;
    };
    if (!_targetScene){
        CCLOG("[LayerSwitcher] addLayer, _targetScene 未註冊");
        return;
    };

    _targetScene->addChild(layer);
    _layerMaps.insert(std::pair<std::string, BaseLayer*>(layerName, layer));
};

void LayerSwitcher::switchLayer(std::string layerName){
    auto iter = _layerMaps.find(layerName);
    if (iter == _layerMaps.end()){
        CCLOG("[LayerSwitcher] switchLayer, %s 不存在", layerName.c_str());
        return;
    };

    if (_currentLayer){       
        _currentLayer->removeLayer();
        _currentLayer = nullptr;
    };
    _currentLayer = iter->second;
    _currentLayer->createLayer();
};
