#include "BaseLayer.h"

bool BaseLayer::init(){
    CCLOG("[BaseLayer] init, 需覆寫");
    return false;
};

void BaseLayer::createLayer(){
    CCLOG("[BaseLayer] createLayer, 需覆寫");  
};

void BaseLayer::removeLayer(){
    CCLOG("[BaseLayer] removeLayer, 需覆寫");
};