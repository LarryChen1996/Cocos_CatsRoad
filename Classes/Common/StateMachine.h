#ifndef __STATE_MACHINE_H__
#define __STATE_MACHINE_H__

#include "cocos2d.h"

typedef std::string StateMachineState;

class StateMachine{  
    public:
        // creator
        static StateMachine* create(StateMachineState initState, void* scope);
        StateMachine(StateMachineState initState, void* scope);

        // function
        void addState(StateMachineState state, std::function<void(bool, float)> stateFunc);
        void transfer(StateMachineState nextState);
        StateMachineState getCurrentState();
        bool isState(StateMachineState state);
        void update(float dt);
    
    private:
        // variable
        void* _scope;
        StateMachineState _currentState;
        StateMachineState _nextState;
        bool _isFirstExec;
        std::map<StateMachineState, std::function<void(bool, float)>> _stateFunc;
};

#endif // __STATE_MACHINE_H__