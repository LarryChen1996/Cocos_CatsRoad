#ifndef __GAMELAYER_H__
#define __GAMELAYER_H__

#include "cocos2d.h"
#include "Common/Layer/BaseLayer.h"
#include "Common/View/Button.h"
#include "Game/Game/Game.h"
#include "Game/View/PauseView.h"
#include "Game/View/ResultView.h"

class GameLayer : public BaseLayer{
    public:
        // creator
        CREATE_FUNC(GameLayer);
        virtual bool init() override;
        virtual void createLayer() override;
        virtual void removeLayer() override;

    private: 
        // class const variable
        const static std::string PAUSE_BTN_NORMAL_FILE_PATH;
        const static std::string PAUSE_BTN_PRESSED_FILE_PATH;

        // variable
        Game* _game;
        Button* _pauseButton;
        PauseView* _pauseView;
        ResultView* _resultView;

        // function
        void _onGameOver();
        void _onPauseViewEvent(PauseViewEvent event);
        void _onReusltViewEvent(ResultViewEvent event);
        Game* _createGame();
        Button* _createPauseButton();
        PauseView* _createPauseView();
        ResultView* _createResultView();
};

#endif // __GAMELAYER_H__