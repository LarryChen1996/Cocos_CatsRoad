#include "Game/View/PauseView.h"
#include "Common/View/Button.h"
#include "Common/View/AudioButton.h"
#include "Common/Config.h"

USING_NS_CC;

const std::map<std::string, PauseViewEvent> PauseView::EVENT = {
    {"CLICK_CONTIMNUE_BUTTON", "CLICK_CONTIMNUE_BUTTON"},
    {"CLICK_HOME_BUTTON", "CLICK_HOME_BUTTON"},
    {"CLICK_RESTART_BUTTON", "CLICK_RESTART_BUTTON"},
};
const std::string PauseView::TITLE_FILE_PATH           = "title/title_pause.png";
const std::string PauseView::CONTINUE_BUTTON_FILE_PATH = "btn/btn_play.png";
const std::string PauseView::HOME_BUTTON_FILE_PATH     = "btn/btn_home.png";
const std::string PauseView::RESTART_BUTTON_FILE_PATH  = "btn/btn_restart.png";
const float PauseView::CONTINUE_BUTTON_SCALE_X         = 1.15;
const float PauseView::CONTINUE_BUTTON_SCALE_Y         = 0.85;

// ----------------------------------------------  Creator  ----------------------------------------------

bool PauseView::init(){
    this->_initVariableWithConfigData();

    _swallowTouchListener = this->_createSwallowTouchListener();
    _title                = this->_createTitle();
    _continueButton       = this->_createContinueButton();
    _homeButton           = this->_createHomeButton();
    _restartButton        = this->_createRestartButton();
    _audioButton          = this->_createAudioButton();
    this->setViewEnabled(false);

    return true;
};

void PauseView::_initVariableWithConfigData(){
    TITLE_POS           = Vec2(Config::WIN_SIZE.width / 2, 515);
    CONTINUE_BUTTON_POS = Vec2(Config::WIN_SIZE.width / 2, 320);
    HOME_BUTTON_POS     = Vec2(Config::WIN_SIZE.width / 2 - 150, 165);
    RESTART_BUTTON_POS  = Vec2(Config::WIN_SIZE.width / 2, 165);
    AUDIO_BUTTON_POS    = Vec2(Config::WIN_SIZE.width / 2 + 150, 165);
};

// ----------------------------------------------  Public  ----------------------------------------------

void PauseView::setEventListener(PauseViewEventListener eventListener){
    _eventListener = eventListener;
};

void PauseView::setViewEnabled(bool enabled){
    this->setVisible(enabled);
    _swallowTouchListener->setSwallowTouches(enabled);       
    _swallowTouchListener->setEnabled(enabled);
    _continueButton->setEnabled(enabled);
    _homeButton->setEnabled(enabled);    
    _restartButton->setEnabled(enabled);
    _audioButton->setEnalbed(enabled);
};

// ----------------------------------------------  Private  ----------------------------------------------

void PauseView::_dispatchEvent(PauseViewEvent event){
    if (_eventListener != nullptr){
        _eventListener(event);
    };
};

/* ------------------------------- */
/* ------- Create Function ------- */
/* ------------------------------- */

EventListenerTouchOneByOne* PauseView::_createSwallowTouchListener(){
    EventListenerTouchOneByOne* swallowTouchListener = EventListenerTouchOneByOne::create();
    swallowTouchListener->onTouchBegan = [](Touch* touch, Event* event){
        return true;
    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(swallowTouchListener, this);
    return swallowTouchListener;
};

Sprite* PauseView::_createTitle(){
    Sprite* title = Sprite::create(PauseView::TITLE_FILE_PATH);
    title->setPosition(TITLE_POS);
    this->addChild(title);
    return title;
};

Button* PauseView::_createContinueButton(){
    Button* continueButton = Button::create(
        PauseView::CONTINUE_BUTTON_FILE_PATH,
        PauseView::CONTINUE_BUTTON_FILE_PATH,
        [this](Ref* sender){ 
            this->_dispatchEvent(PauseView::EVENT.at("CLICK_CONTIMNUE_BUTTON")); 
        }
    );
    continueButton->setPosition(PauseView::CONTINUE_BUTTON_POS);
    this->addChild(continueButton);
    return continueButton;
};

Button* PauseView::_createHomeButton(){
    Button* homeButton = Button::create(
        PauseView::HOME_BUTTON_FILE_PATH,
        PauseView::HOME_BUTTON_FILE_PATH,
        [this](Ref* sender){ 
            this->_dispatchEvent(PauseView::EVENT.at("CLICK_HOME_BUTTON")); 
        }
    );
    homeButton->setPosition(PauseView::HOME_BUTTON_POS);
    this->addChild(homeButton);
    return homeButton;
};

Button* PauseView::_createRestartButton(){
    Button* restartButton = Button::create(
        PauseView::RESTART_BUTTON_FILE_PATH,
        PauseView::RESTART_BUTTON_FILE_PATH,
        [this](Ref* sender){
            this->_dispatchEvent(PauseView::EVENT.at("CLICK_RESTART_BUTTON")); 
        }
    );
    restartButton->setPosition(PauseView::RESTART_BUTTON_POS);
    this->addChild(restartButton);
    return restartButton;
};

AudioButton* PauseView::_createAudioButton(){
    AudioButton* audioButton = AudioButton::create();
    audioButton->setPosition(AUDIO_BUTTON_POS);
    this->addChild(audioButton);
    return audioButton;
};