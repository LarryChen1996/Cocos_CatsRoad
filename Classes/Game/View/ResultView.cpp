#include "Game/View/ResultView.h"
#include "Common/View/Button.h"
#include "Common/Config.h"
#include "Common/Tools.h"

USING_NS_CC;

/* ---------------- Public Const ---------------- */

const std::map<std::string, ResultViewEvent> ResultView::EVENT = {
    {"CLICK_RESTART_BUTTON", "CLICK_RESTART_BUTTON"},
    {"CLICK_HOME_BUTTON", "CLICK_HOME_BUTTON"},
};

/* ---------------- Private Const ---------------- */

const std::string ResultView::SCORE_TEXT               = "分數 : %d";
const std::string ResultView::BEST_SCORE_TEXT          = "最高紀錄 : %d";
const std::string ResultView::TITLE_FILE_PATH          = "title/title_game_over.png";
const std::string ResultView::RESTART_BUTTON_FILE_PATH = "btn/btn_restart.png";
const std::string ResultView::HOME_BUTTON_FILE_PATH    = "btn/btn_home.png";
const std::string ResultView::NEW_RECORD_FILE_PATH     = "new_record.png";

/* ---------------------------------------------  Creator  --------------------------------------------- */

bool ResultView::init(){
    this->_initVariableWithConfigData();

    _swallowTouchListener = this->_createSwallowTouchListener();
    _title                = this->_createTitle();
    _scoreLabel           = this->_createScoreLabel();
    _bestScoreLabel       = this->_createBestScoreLabel();
    _restartButton        = this->_createRestartButton();
    _homeButton           = this->_createHomeButton();
    this->_setViewEnabled(false);

    return true;
};

void ResultView::_initVariableWithConfigData(){
    auto CENTER_X = Config::CENTER_X;

    RESTART_BUTTON_POS_START   = Vec2(CENTER_X + 75, - 200);
    RESTART_BUTTON_POS_END     = Vec2(CENTER_X + 75, 100);
    HOME_BUTTON_POS_START      = Vec2(CENTER_X - 75, - 200);
    HOME_BUTTON_POS_END        = Vec2(CENTER_X -75, 100);
    TITLE_POS_START            = Vec2(-350, 435);
    TITLE_POS_END              = Vec2(CENTER_X, 435);
    SCORE_LABEL_POS_START      = Vec2(-350, 275);
    SCORE_LABEL_POS_END        = Vec2(CENTER_X, 275);
    BEST_SCORE_LABEL_POS_START = Vec2(-350, 210);
    BEST_SCORE_LABEL_POS_END   = Vec2(CENTER_X, 210);
    NEW_RECORD_POS             = Vec2(CENTER_X - 275, 275);
};

/* ---------------------------------------------  Public  --------------------------------------------- */

void ResultView::setEventListener(ResultViewEventListener eventListener){
    _eventListener = eventListener;
};

void ResultView::playResultAnimaion(int score, int bestScore, bool isPlayBestScoreAni){
    this->_setViewEnabled(true);

    _scoreLabel->setString(Tools::formatString(ResultView::SCORE_TEXT, score));
    _bestScoreLabel->setString(Tools::formatString(ResultView::BEST_SCORE_TEXT, bestScore));

    _restartButton->runAction(JumpTo::create(0.6, RESTART_BUTTON_POS_END, 250, 1));
    _homeButton->runAction(JumpTo::create(0.6, HOME_BUTTON_POS_END, 250, 1));
    this->runAction(FadeTo::create(0.5, 185));
    this->runAction(Sequence::create(
        CallFunc::create([this](){
            _title->runAction(MoveTo::create(0.4, TITLE_POS_END));
        }),
        DelayTime::create(0.15),
        CallFunc::create([this](){
            _scoreLabel->runAction(MoveTo::create(0.4, SCORE_LABEL_POS_END));
        }),
        DelayTime::create(0.15),
        CallFunc::create([this](){
            _bestScoreLabel->runAction(MoveTo::create(0.4, BEST_SCORE_LABEL_POS_END));
        }),
        DelayTime::create(0.6),
        isPlayBestScoreAni ? CallFunc::create([this](){ this->_playNewRecordAnimation(); }) : nullptr,
        nullptr
    ));
};

/* ---------------------------------------------  Private  --------------------------------------------- */

void ResultView::_setViewEnabled(bool enabled){
    this->setVisible(enabled);
    _swallowTouchListener->setSwallowTouches(enabled);   
    _swallowTouchListener->setEnabled(enabled);    
    _homeButton->setEnabled(enabled);    
    _restartButton->setEnabled(enabled);
};

void ResultView::_dispatchEvent(ResultViewEvent event){
    if (_eventListener != nullptr){
        _eventListener(event);
    };
};

void ResultView::_playNewRecordAnimation(){
    Sprite* newRecord = Sprite::create(ResultView::NEW_RECORD_FILE_PATH);
    newRecord->setPosition(NEW_RECORD_POS);
    newRecord->setOpacity(0);
    this->addChild(newRecord);
    newRecord->runAction(FadeIn::create(0.5));
};

/* ------------------------------- */
/* ------- Create Function ------- */
/* ------------------------------- */

EventListenerTouchOneByOne* ResultView::_createSwallowTouchListener(){
    EventListenerTouchOneByOne* swallowTouchListener = EventListenerTouchOneByOne::create();
    swallowTouchListener->onTouchBegan = [](Touch* touch, Event* event){
        return true;
    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(swallowTouchListener, this);
    return swallowTouchListener;
};

Sprite* ResultView::_createTitle(){
    Sprite* title = Sprite::create(ResultView::TITLE_FILE_PATH);
    title->setPosition(TITLE_POS_START);
    this->addChild(title);
    return title;
};

Label* ResultView::_createScoreLabel(){
    Label* scoreLabel = Label::createWithBMFont(Config::GAME_BMFONT_FILA_PATH, "");
    scoreLabel->setPosition(SCORE_LABEL_POS_START);
    this->addChild(scoreLabel);
    return scoreLabel;
};

Label* ResultView::_createBestScoreLabel(){
    Label* bestScoreLabel = Label::createWithBMFont(Config::GAME_BMFONT_FILA_PATH, "");
    bestScoreLabel->setPosition(BEST_SCORE_LABEL_POS_START);
    this->addChild(bestScoreLabel);
    return bestScoreLabel;
};

Button* ResultView::_createRestartButton(){
    Button* restartButton = Button::create(
        ResultView::RESTART_BUTTON_FILE_PATH,
        ResultView::RESTART_BUTTON_FILE_PATH,
        [this](Ref* sender){
            this->_dispatchEvent(ResultView::EVENT.at("CLICK_RESTART_BUTTON")); 
        }
    );
    restartButton->setPosition(RESTART_BUTTON_POS_START);
    this->addChild(restartButton);
    return restartButton;
};

Button* ResultView::_createHomeButton(){
    Button* homeButton = Button::create(
        ResultView::HOME_BUTTON_FILE_PATH,
        ResultView::HOME_BUTTON_FILE_PATH,
        [this](Ref* sender){
            this->_dispatchEvent(ResultView::EVENT.at("CLICK_HOME_BUTTON")); 
        }
    );
    homeButton->setPosition(HOME_BUTTON_POS_START);
    this->addChild(homeButton);
    return homeButton;
};

