#ifndef __PAUSE_VIEW_H__
#define __PAUSE_VIEW_H__

#include "cocos2d.h"
#include "Common/View/Button.h"
#include "Common/View/AudioButton.h"

typedef std::string PauseViewEvent;
typedef std::function<void(PauseViewEvent)> PauseViewEventListener;

class PauseView : public cocos2d::Layer{
    public:
        // class const variable
        const static std::map<std::string, PauseViewEvent> EVENT;

        // creator
        CREATE_FUNC(PauseView);
        virtual bool init();

        // function
        void setEventListener(PauseViewEventListener eventListener);
        void setViewEnabled(bool enabled);

    private:
        // class const vairable
        cocos2d::Vec2 TITLE_POS;
        cocos2d::Vec2 CONTINUE_BUTTON_POS;
        cocos2d::Vec2 HOME_BUTTON_POS;
        cocos2d::Vec2 RESTART_BUTTON_POS;
        cocos2d::Vec2 AUDIO_BUTTON_POS;
        void _initVariableWithConfigData();

        const static std::string TITLE_FILE_PATH;
        const static std::string CONTINUE_BUTTON_FILE_PATH;
        const static std::string HOME_BUTTON_FILE_PATH;
        const static std::string RESTART_BUTTON_FILE_PATH;
        const static float CONTINUE_BUTTON_SCALE_X;
        const static float CONTINUE_BUTTON_SCALE_Y;

        // variable
        PauseViewEventListener _eventListener;
        cocos2d::EventListenerTouchOneByOne* _swallowTouchListener;
        cocos2d::Sprite* _title;
        Button* _continueButton;
        Button* _homeButton;
        Button* _restartButton;
        AudioButton* _audioButton;

        // function
        void _dispatchEvent(PauseViewEvent);
        cocos2d::EventListenerTouchOneByOne* _createSwallowTouchListener();
        cocos2d::Sprite* _createTitle();
        Button* _createContinueButton();
        Button* _createHomeButton();
        Button* _createRestartButton();
        AudioButton* _createAudioButton();
};

#endif // __PAUSE_VIEW_H__