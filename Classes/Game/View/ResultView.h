#ifndef __RESULT_VIEW_H__
#define __RESULT_VIEW_H__

#include "cocos2d.h"
#include "Common/View/Button.h"

typedef std::string ResultViewEvent;
typedef std::function<void(ResultViewEvent)> ResultViewEventListener;

class ResultView : public cocos2d::Layer{
    public:
        // creator
        CREATE_FUNC(ResultView);
        virtual bool init();

        // class const variable
        const static std::map<std::string, ResultViewEvent> EVENT;

        // function
        void setEventListener(ResultViewEventListener eventListener);
        void playResultAnimaion(int score, int bestScore, bool isPlayBestScoreAni);

    private:
        // class const variable
        const static std::string SCORE_TEXT;
        const static std::string BEST_SCORE_TEXT;
        const static std::string TITLE_FILE_PATH;
        const static std::string RESTART_BUTTON_FILE_PATH;
        const static std::string HOME_BUTTON_FILE_PATH;
        const static std::string NEW_RECORD_FILE_PATH;

        cocos2d::Vec2 RESTART_BUTTON_POS_START;
        cocos2d::Vec2 RESTART_BUTTON_POS_END;
        cocos2d::Vec2 HOME_BUTTON_POS_START;
        cocos2d::Vec2 HOME_BUTTON_POS_END;
        cocos2d::Vec2 TITLE_POS_START;
        cocos2d::Vec2 TITLE_POS_END;
        cocos2d::Vec2 SCORE_LABEL_POS_START;
        cocos2d::Vec2 SCORE_LABEL_POS_END;
        cocos2d::Vec2 BEST_SCORE_LABEL_POS_START;
        cocos2d::Vec2 BEST_SCORE_LABEL_POS_END;
        cocos2d::Vec2 NEW_RECORD_POS;
        void _initVariableWithConfigData();

        // variable
        cocos2d::EventListenerTouchOneByOne* _swallowTouchListener;
        Button* _restartButton;
        Button* _homeButton;
        cocos2d::Sprite* _title;
        cocos2d::Label* _scoreLabel;
        cocos2d::Label* _bestScoreLabel;
        ResultViewEventListener _eventListener;

        // function
        void _setViewEnabled(bool enabled);
        void _dispatchEvent(ResultViewEvent event);
        void _playNewRecordAnimation();
        cocos2d::EventListenerTouchOneByOne* _createSwallowTouchListener();
        cocos2d::Sprite* _createTitle();
        cocos2d::Label* _createScoreLabel();
        cocos2d::Label* _createBestScoreLabel();
        Button* _createRestartButton();
        Button* _createHomeButton();
};

#endif // __RESULT_VIEW_H__