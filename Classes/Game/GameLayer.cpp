#include "Game/GameLayer.h"
#include "Game/Game/Game.h"
#include "Game/View/PauseView.h"
#include "Game/View/ResultView.h"
#include "Common/Config.h"
#include "Common/View/Button.h"
#include "Common/Layer/LayerSwitcher.h"

USING_NS_CC;

const std::string GameLayer::PAUSE_BTN_NORMAL_FILE_PATH = "btn/btn_pause.png";
const std::string GameLayer::PAUSE_BTN_PRESSED_FILE_PATH = "btn/btn_pause.png";

// ----------------------------------------------  creator  ----------------------------------------------

bool GameLayer::init(){
    return true;
};

void GameLayer::createLayer(){
    _game        = this->_createGame();
    _pauseButton = this->_createPauseButton();
    _pauseView   = this->_createPauseView();
    _resultView  = this->_createResultView();
};

void GameLayer::removeLayer(){
    this->removeAllChildren();
};

// ----------------------------------------------  Private  ----------------------------------------------

/* ------------------------------- */
/* -------- Event Function ------- */
/* ------------------------------- */

void GameLayer::_onGameOver(){
    // TODO
    const int BEST_SCORE = 75;

    int score = _game->getScore();
    bool isNewRecord = score > BEST_SCORE;
    _resultView->playResultAnimaion(
        score,
        isNewRecord ? BEST_SCORE : BEST_SCORE,
        isNewRecord
    );
};

void GameLayer::_onPauseViewEvent(PauseViewEvent event){
    if (event == PauseView::EVENT.at("CLICK_CONTIMNUE_BUTTON")){
        _pauseView->setViewEnabled(false);
        _game->resumeGame();

    }else if (event == PauseView::EVENT.at("CLICK_HOME_BUTTON")){
        LayerSwitcher::getInstance()->switchLayer(Config::LOBBY_NAME);

    }else if (event == PauseView::EVENT.at("CLICK_RESTART_BUTTON")){
        LayerSwitcher::getInstance()->switchLayer(Config::GAME_NAME);

    };
};

void GameLayer::_onReusltViewEvent(ResultViewEvent event){
    if (event == ResultView::EVENT.at("CLICK_RESTART_BUTTON")){
        LayerSwitcher::getInstance()->switchLayer(Config::GAME_NAME);

    }else if (event == ResultView::EVENT.at("CLICK_HOME_BUTTON")){
        LayerSwitcher::getInstance()->switchLayer(Config::LOBBY_NAME);

    };
};

/* ------------------------------- */
/* ------- Create Function ------- */
/* ------------------------------- */

Game* GameLayer::_createGame(){
    Game* game = Game::create();
    game->setGameOverCallback(CC_CALLBACK_0(GameLayer::_onGameOver, this));
    this->addChild(game);
    return game;
};

Button* GameLayer::_createPauseButton(){
    const Vec2 BTN_START_POS = Vec2(Config::WIN_SIZE.width - 130, 575);

    Button* pauseButton = Button::create(
        GameLayer::PAUSE_BTN_NORMAL_FILE_PATH,
        GameLayer::PAUSE_BTN_PRESSED_FILE_PATH,
        [this](Ref* sender){
            _pauseView->setViewEnabled(true);
            _game->pauseGame();
        }
    );
    pauseButton->setPosition(BTN_START_POS);
    this->addChild(pauseButton);

    return pauseButton;
};

PauseView* GameLayer::_createPauseView(){
    PauseView* pauseView = PauseView::create();
    pauseView->setEventListener(CC_CALLBACK_1(GameLayer::_onPauseViewEvent, this));
    this->addChild(pauseView);
    return pauseView;
};

ResultView* GameLayer::_createResultView(){
    ResultView* resultView = ResultView::create();
    resultView->setEventListener(CC_CALLBACK_1(GameLayer::_onReusltViewEvent, this));
    this->addChild(resultView);
    return resultView;
};
