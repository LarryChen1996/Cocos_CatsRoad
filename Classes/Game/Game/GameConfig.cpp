#include "Game/Game/GameConfig.h"


/* ---------------- Public Const ---------------- */

const std::map<std::string, std::string> GameConfig::ANIMAL_TYPE = {
    {"CAT", "CAT"},
    {"LION", "LION"},
    {"LEOPARD_CAT", "LEOPARD_CAT"},
    {"CHEETAH", "CHEETAH"},
};

/* ---------------------------------------------  Public Function  --------------------------------------------- */

AnimalDetail GameConfig::getAnimalDetail(AnimalType animalType){
    return GameConfig::_ANIMAL_DETAIL.at(animalType);
};

/* ---------------- Private Const ---------------- */

const AnimalDetail GameConfig::_CAT_DETAIL = AnimalDetail{
    35,                    // WALK_SPEED
    110,                   // RUN_SPEED
    AnimalAnimationDetail{ // FORWARD_ANIMATION_DETAIL
        "cat_forward_%d.png",   // FILE_NAME
        4,                      // TOTAL_FRAMES
        0.2,                    // WALK_RATE
        0.1,                    // RUN_RATE
        2,                      // IDLE_FRAME
    },
    AnimalAnimationDetail{ // BACKWWARD_ANIMATION_DETAIL
        "cat_backward_%d.png",  // FILE_NAME
        4,                      // TOTAL_FRAMES
        0.2,                    // WALK_RATE
        0.1,                    // RUN_RATE
        2,                      // IDLE_FRAME
    },
};

const AnimalDetail GameConfig::_LION_DETAIL = AnimalDetail{
    70,
    210,
    AnimalAnimationDetail{
        "lion_forward_%d.png",
        4,
        0.2,
        0.1,
        2,
    },
    AnimalAnimationDetail{
        "lion_backward_%d.png",
        4,
        0.2,
        0.1,
        2,
    },
};

const AnimalDetail GameConfig::_LEOPARD_CAT_DETAIL = AnimalDetail{
    110,
    330,
    AnimalAnimationDetail{
        "leopard_cat_forward_%d.png",
        4,
        0.2,
        0.1,
        2,
    },
    AnimalAnimationDetail{
        "leopard_cat_backward_%d.png",
        4,
        0.2,
        0.1,
        2,
    },
};

const AnimalDetail GameConfig::_CHEETAH_DETAIL = AnimalDetail{
    140,
    420,
    AnimalAnimationDetail{
        "cheetah_forward_%d.png",
        4,
        0.2,
        0.1,
        2,
    },
    AnimalAnimationDetail{
        "cheetah_backward_%d.png",
        4,
        0.2,
        0.1,
        2,
    },
};

const std::map<AnimalType, AnimalDetail> GameConfig::_ANIMAL_DETAIL = {
    {GameConfig::ANIMAL_TYPE.at("CAT"), GameConfig::_CAT_DETAIL},
    {GameConfig::ANIMAL_TYPE.at("LION"), GameConfig::_LION_DETAIL},
    {GameConfig::ANIMAL_TYPE.at("LEOPARD_CAT"), GameConfig::_LEOPARD_CAT_DETAIL},
    {GameConfig::ANIMAL_TYPE.at("CHEETAH"), GameConfig::_CHEETAH_DETAIL},
};