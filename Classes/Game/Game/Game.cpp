#include "Game/Game/Game.h"
#include "Game/Game/Model/GameModel.h"
#include "Game/Game/View/GameView.h"
#include "Game/Game/View/GameUIView.h"
#include "editor-support/cocostudio/SimpleAudioEngine.h"

/* ---------------------------------------------  Creator  --------------------------------------------- */

bool Game::init(){
    _isGameOver = false;
    _gameModel  = this->_createGameModel();
    _gameView   = this->_createGameView();
    _gameUIView = this->_createGameUIView(_gameModel->getHealth());
    this->scheduleUpdate();

    return true;
};

/* ---------------------------------------------  Public  --------------------------------------------- */

void Game::update(float dt){
    _gameModel->update(dt);
    _gameView->update(dt);
};

void Game::setGameOverCallback(std::function<void()> callback){
    _gameOverCallback = callback;
};

void Game::pauseGame(){
    this->unscheduleUpdate();
};

void Game::resumeGame(){
    this->scheduleUpdate();
};

int Game::getScore(){
    return _gameModel->getScore();
};

/* ---------------------------------------------  Private  --------------------------------------------- */

/* ------------------------------- */
/* --- Game View Event Function -- */
/* ------------------------------- */

void Game::_onGameViewEventListener(GameViewEvent event, void* customData){
    if (_isGameOver){
        return;
    };

    if (event == GameView::EVENT.at("ANIMAL_OUT_OF_SCREEN")){
        this->_onViewAnimalOutOfScreen(static_cast<AnimalType*>(customData));

    }else if (event == GameView::EVENT.at("CAR_OUT_OF_SCREEN")){
        this->_onViewCarOutOfScreen();

    }else if (event == GameView::EVENT.at("HEART_DISAPPEAR")){
        this->_onViewHeartDisappear();
    
    }else if (event == GameView::EVENT.at("ANIMAL_COLLIDE_CAR")){
        this->_onViewAnimalCollideCar();

    }else if (event == GameView::EVENT.at("ANIMAL_COLLIDE_HEART")){
        this->_onViewAnimalCollideHeart();

    };
};

void Game::_onViewAnimalOutOfScreen(AnimalType* animalType){
    _gameModel->addScore(*animalType);
    _gameUIView->setScore(_gameModel->getScore());
};

void Game::_onViewCarOutOfScreen(){
    _gameModel->minusCarsAmount();
};

void Game::_onViewHeartDisappear(){
    _gameModel->minusHeartsAmount();
};

void Game::_onViewAnimalCollideCar(){
    _gameModel->minusHealth();
    _gameUIView->setHealth(_gameModel->getHealth());
};

void Game::_onViewAnimalCollideHeart(){
    _gameModel->addHealth();
    _gameUIView->setHealth(_gameModel->getHealth());
};

/* ------------------------------- */
/* -- Game Model Event Function -- */
/* ------------------------------- */

void Game::_onGameModelEventListener(GameModelEvent event, void* customData){
    if (_isGameOver){
        return;
    };

    if (event == GameModel::EVENT.at("CREATE_CAR")){
        this->_onModelCreateCar();

    }else if (event == GameModel::EVENT.at("CREATE_ANIMAL")){
        this->_onModelCreateAnimal(static_cast<AnimalType*>(customData));

    }else if (event == GameModel::EVENT.at("CREATE_HEART")){
        this->_onModelCreateHeart();

    }else if (event == GameModel::EVENT.at("NEW_ANIMAL_APPEAR")){
        this->_onModelNewAnimalAppear();

    }else if (event == GameModel::EVENT.at("OUT_OF_HEALTH")){
        this->_onModelOutOfHealth();

    }
};

void Game::_onModelCreateCar(){
    _gameView->createRandomCar();
};

void Game::_onModelCreateAnimal(AnimalType* animalType){
    _gameView->createAnimal(*animalType);
};

void Game::_onModelCreateHeart(){
    _gameView->createRandomHeartOnRoad();
};

void Game::_onModelNewAnimalAppear(){
    _gameUIView->showNewAnimalComing();
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/new_animal_appear.mp3");
};

void Game::_onModelOutOfHealth(){
    _isGameOver = true;
    _gameView->removeAllAnimals();
    this->unscheduleUpdate();
    if (_gameOverCallback != nullptr){
        _gameOverCallback();
    };
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/game_over.mp3");
};

/* ------------------------------- */
/* ------- Create Function ------- */
/* ------------------------------- */

GameModel* Game::_createGameModel(){
    GameModel* gameModel = GameModel::create();
    gameModel->setEventListener(CC_CALLBACK_2(Game::_onGameModelEventListener, this));
    return gameModel;
};

GameView* Game::_createGameView(){
    GameView* gameView = GameView::create();
    gameView->setEventListener(CC_CALLBACK_2(Game::_onGameViewEventListener, this));
    this->addChild(gameView);
    return gameView;
};

GameUIView* Game::_createGameUIView(int maxHealth){
   GameUIView* gameUIView = GameUIView::create(maxHealth);
   this->addChild(gameUIView);
   return gameUIView;
};