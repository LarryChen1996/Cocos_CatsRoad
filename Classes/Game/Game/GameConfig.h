#ifndef __GAME_CONFIG_H__
#define __GAME_CONFIG_H__

#include "cocos2d.h"

typedef std::string AnimalType;

typedef struct {
    std::string FILE_NAME;
    int TOTAL_FRAMES;
    float WALK_RATE;
    float RUN_RATE;
    int IDLE_FRAME;
} AnimalAnimationDetail;

typedef struct {
    float WALK_SPEED;
    float RUN_SPEED;
    AnimalAnimationDetail FORWARD_ANIMATION_DETAIL;
    AnimalAnimationDetail BACKWWARD_ANIMATION_DETAIL;
} AnimalDetail;

class GameConfig{
    public:
        // class const variable
        const static std::map<std::string, AnimalType> ANIMAL_TYPE;

        // function
        static AnimalDetail getAnimalDetail(AnimalType animalType);

    private:
        const static AnimalDetail _CAT_DETAIL;
        const static AnimalDetail _LION_DETAIL;
        const static AnimalDetail _LEOPARD_CAT_DETAIL;
        const static AnimalDetail _CHEETAH_DETAIL;
        const static std::map<AnimalType, AnimalDetail> _ANIMAL_DETAIL;

};

#endif // __GAME_CONFIG_H__