#ifndef __GAME_H__
#define __GAME_H__

#include "cocos2d.h"
#include "Model/GameModel.h"
#include "Game/Game/Model/GameModel.h"
#include "Game/Game/View/GameView.h"
#include "Game/Game/View/GameUIView.h"

class Game : public cocos2d::Layer{
    public:
        // creator
        CREATE_FUNC(Game);
        virtual bool init() override;

        // function
        virtual void update(float dt) override;
        void setGameOverCallback(std::function<void()> callback);
        void pauseGame();
        void resumeGame();
        int getScore();
    private: 
        // variable
        bool _isGameOver;
        GameModel* _gameModel;
        GameView* _gameView;
        GameUIView* _gameUIView;
        std::function<void()> _gameOverCallback;

        // function
        void _onGameViewEventListener(GameViewEvent event, void* customData = nullptr);
        void _onViewAnimalOutOfScreen(AnimalType* animalType);
        void _onViewCarOutOfScreen();
        void _onViewHeartDisappear();
        void _onViewAnimalCollideCar();
        void _onViewAnimalCollideHeart();
        void _onGameModelEventListener(GameModelEvent event, void* customData = nullptr);
        void _onModelCreateCar();
        void _onModelCreateAnimal(AnimalType* animalType);
        void _onModelCreateHeart();
        void _onModelNewAnimalAppear();
        void _onModelOutOfHealth();
        GameModel* _createGameModel();
        GameView* _createGameView();
        GameUIView* _createGameUIView(int maxHealth);;
};

#endif // __GAME_H__