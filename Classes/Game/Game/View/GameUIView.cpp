#include "Game/Game/View/GameUIView.h"
#include "Common/View/ItemBar.h"
#include "Common/Config.h"
#include "Common/Tools.h"

USING_NS_CC;

// Heart Bar
const std::string GameUIView::HEART_FILE_PATH       = "heart/ui_heart.png";
const std::string GameUIView::HEART_EMPTY_FILE_PATH = "heart/ui_heart_empty.png";
const Vec2 GameUIView::HEART_POS_OFFSET             = Vec2(75, 0);
const Vec2 GameUIView::HEART_BAR_POS                = Vec2(100, 575);

// Score Label
const Vec2 GameUIView::SCORE_LABEL_POS         = Vec2(145, 515);
const Color3B GameUIView::SCORE_LABEL_COLOR    = Color3B(255, 255, 0);
const std::string GameUIView::SCORE_LABEL_TEXT = "分數 : %d";

// New Animal Label
const float GameUIView::NEW_ANIMAL_LAVEL_SCALE      = 1.8;
const std::string GameUIView::NEW_ANIMAL_LABEL_TEXT = "發現新動物!";

// Red Screen Border
const std::string GameUIView::RED_SCREEN_BORDER_FILE_PATH = "red_screen_border.png";
const float GameUIView::RED_SCREEN_BORDER_FADE_DURATION   = 0.5;

/* ---------------------------------------------  Creator  --------------------------------------------- */

GameUIView* GameUIView::create(int maxHealth){
    GameUIView* instance = new(std::nothrow) GameUIView(maxHealth);
    
    if (instance){ 
        instance->autorelease();
        return instance;
    }else{
        delete instance;
        instance = nullptr;
        return nullptr;
    };
};

GameUIView::GameUIView(int maxHealth){
    NEW_ANIMAL_LABEL_POS  = Vec2(Config::CENTER_X, Config::CENTER_Y - 75);
    RED_SCREEN_BORDER_POS = Config::CENTER;

    _heartBar        = this->_createHeartBar(maxHealth);
    _scoreLabel      = this->_createScoreLabel(maxHealth);
    _newAnimalLabel  = this->_createNewAnimalLabel();
    _redScreenBorder = this->_createRedScreenBorder();

    this->setScore(0);
    this->setHealth(maxHealth);
};

/* ---------------------------------------------  Public  --------------------------------------------- */

void GameUIView::setHealth(int health){
    _heartBar->setItemAmount(health);
    _redScreenBorder->setVisible(health == 1);
};

void GameUIView::setScore(int score){
    _scoreLabel->setString(Tools::formatString(SCORE_LABEL_TEXT, score));
};

void GameUIView::showNewAnimalComing(){
    _newAnimalLabel->setPosition(NEW_ANIMAL_LABEL_POS);
    _newAnimalLabel->setScale(NEW_ANIMAL_LAVEL_SCALE);
    _newAnimalLabel->setOpacity(0);
    _newAnimalLabel->setVisible(true);

    _newAnimalLabel->runAction(Sequence::create(
        Spawn::create(
            FadeIn::create(0.2),
            MoveBy::create(0.2, Vec2(0, 50))
            ,nullptr
        ),
        MoveBy::create(1.5, Vec2(0, 50)),
        Spawn::create(
            FadeOut::create(0.2),
            EaseSineIn::create(MoveBy::create(0.2, Vec2(0, 35))),
            nullptr
        ),
        CallFunc::create(
            [this](){ _newAnimalLabel->setVisible(false); }
        ),
        nullptr
    ));
};

/* ---------------------------------------------  Private  --------------------------------------------- */

/* -------------------------------- */
/* -----------  Create  ----------- */
/* -------------------------------- */

ItemBar* GameUIView::_createHeartBar(int maxHealth){
    ItemBar* heartBar = ItemBar::create(maxHealth, HEART_FILE_PATH, HEART_EMPTY_FILE_PATH);
    heartBar->setItemOffset(HEART_POS_OFFSET);
    heartBar->setPosition(HEART_BAR_POS);
    this->addChild(heartBar);
    return heartBar;
};

Label* GameUIView::_createScoreLabel(int maxHealth){
    Label* scoreLabel = Label::createWithBMFont(Config::GAME_BMFONT_FILA_PATH, "");
    scoreLabel->setColor(SCORE_LABEL_COLOR);
    scoreLabel->setPosition(SCORE_LABEL_POS);
    this->addChild(scoreLabel);
    return scoreLabel;
};

Label* GameUIView::_createNewAnimalLabel(){
    Label* newAnimalLabel = Label::createWithBMFont(Config::GAME_BMFONT_FILA_PATH, NEW_ANIMAL_LABEL_TEXT);
    newAnimalLabel->setPosition(NEW_ANIMAL_LABEL_POS);
    newAnimalLabel->setScale(NEW_ANIMAL_LAVEL_SCALE);
    newAnimalLabel->setOpacity(0);
    newAnimalLabel->setVisible(false);
    this->addChild(newAnimalLabel);
    return newAnimalLabel;
};

Sprite* GameUIView::_createRedScreenBorder(){
    Sprite* redScreenBorder = Sprite::create(RED_SCREEN_BORDER_FILE_PATH);
    redScreenBorder->setPosition(RED_SCREEN_BORDER_POS);
    redScreenBorder->setVisible(false);
    this->addChild(redScreenBorder);

    redScreenBorder->runAction(
        RepeatForever::create(
            Sequence::create(
                FadeIn::create(RED_SCREEN_BORDER_FADE_DURATION),
                FadeOut::create(RED_SCREEN_BORDER_FADE_DURATION),
                nullptr
            )
        )
    );

    return redScreenBorder;
};