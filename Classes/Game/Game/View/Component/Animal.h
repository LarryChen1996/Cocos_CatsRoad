#ifndef __ANIMAL_H__
#define __ANIMAL_H__

#include "cocos2d.h"
#include "Game/Game/GameConfig.h"
#include "Common/StateMachine.h"

typedef std::string AnimalType;
typedef std::string AnimalState;
typedef struct {
    float x;
    float y;
    float width;
    float height;
} AnimalRectDiff;

class Animal : public cocos2d::Node{
    public:
        // class const variable
        const static std::map<std::string, AnimalState> STATE;

        // creator
        static Animal* create(AnimalType animalType, bool isFaceForward); 
        Animal(AnimalType animalType, bool isFaceForward);

        // function
        virtual void update(float dt) override;
        void changeState(AnimalState state);
        AnimalType getAnimalType();
        void setOutOfScreenCallback(std::function<void(Animal*)> callback);
        void setBorderY(float min, float max);
        cocos2d::Rect getCollisionRect();
        cocos2d::Rect getTouchRect();
        
    private:
        // class const variable
        const static std::map<std::string, int> ZORDER;
        const static std::string CHOOSED_FRAME_FILE_PATH;
        const static std::string IDLE_SIGN_FILE_PATH;
        const static std::string IDLE_PROGRESS_TIMER_FILE_PATH;
        const static std::string AFTER_GLOW_FILE_PATH;
        const static std::string EXPLOSION_CIRCLE_FILE_PAHT;
        const static cocos2d::Vec2 IDLE_SIGN_POS_OFFSET;
        const static cocos2d::Vec2 IDLE_PROGRESS_TIMER_POS_OFFSET;
        const static cocos2d::Vec2 AFTER_GLOW_POS_OFFSET;
        const static AnimalRectDiff COLLISION_RECT_DIFF;
        const static float AFTER_GLOW_DECREASE_SCALE_PER_SECOND;
        const static float AFTER_GLOW_DECREASE_OPACITY_PER_SECOND;
        const static float AFTER_GLOW_CREATE_TIME;
        const static float MAX_IDLE_TIME;
        const static float START_SHOW_IDLE_EFFECT_TIME;
        const static float MAX_PROGRESS_TIMER_PERCENTAGE;
        const static float DEFAULT_MAX_BORDER_Y;
        const static float DEFAULT_MIN_BORDER_Y;
        const static float TRIGGER_RUN_TOUCH_DELTA_DISTANCE;

        // variable
        AnimalType _animalType;
        AnimalDetail _animalDetail;
        bool _isFaceForward;
        float _afterglowCreateTime;
        float _idleTime;
        float _minBorderY;
        float _maxBorderY;
        bool _isIdleEffectPlayed;
        std::function<void(Animal*)> _outOfScreenCallback; 
        StateMachine* _stateMachine;
        std::vector<cocos2d::Sprite*> _afterglows;
        std::vector<cocos2d::Vec2> _afterglowsPos;
        cocos2d::Sprite* _animalSprite;
        cocos2d::Sprite* _choosedFrame;
        cocos2d::Sprite* _idleSign;
        cocos2d::ProgressTimer* _idleProgressTimer;
        cocos2d::EventListenerTouchOneByOne* _touchListener;

        // function
        void _onWalkState(bool isFirstExec, float dt);
        void _onRunState(bool isFirstExec, float dt);
        void _onIdleState(bool isFirstExec, float dt);
        void _onDieState(bool isFirstExec, float dt);
        void _playExplosionCircleAnimation();
        void _updateIdleStatus();
        void _updateIdleProgressTimer();
        void _createAfterglow(float dt);
        void _updateAfterglows(float dt);
        void _setupToIdleFrame();
        void _refreshPosition(float speed, float dt);
        bool _onTouchBegan(cocos2d::Touch* pTouch, cocos2d::Event* pEvent);
        void _onTouchMoved(cocos2d::Touch* pTouch, cocos2d::Event* pEvent);
        void _onTouchEnded(cocos2d::Touch* pTouch, cocos2d::Event* pEvent);

        cocos2d::FiniteTimeAction* _getStateAnimation(AnimalState state);

        StateMachine* _createStateMachine();
        cocos2d::Sprite* _createAnimalSprite(AnimalType animalType, bool isFaceForward);
        cocos2d::Sprite* _createChoosedFrame();
        cocos2d::Sprite* _createIdleSign();
        cocos2d::ProgressTimer* _createIdleProgressTimer();
        cocos2d::EventListenerTouchOneByOne* _registerScriptTouch();
        void _resetComponentPosition(cocos2d::Size animalSpriteSize);
};
    
#endif // __ANIMAL_H__