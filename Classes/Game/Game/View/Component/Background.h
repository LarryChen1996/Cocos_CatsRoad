#ifndef __BACKGROUND_H__
#define __BACKGROUND_H__

#include "cocos2d.h"

class Background : public cocos2d::Node{
    public:
        // creator
        CREATE_FUNC(Background);
        virtual bool init();

        // function
        cocos2d::Node* getRoadNode();

    private:
        // class const static
        const static std::string ROAD_BG_FILE_PATH;
        const static std::string TREE_FILE_PATH;
        const static std::string BENCH_FILE_PATH;
        const static std::string LAMP_FILE_PATH;
        const static float TREE_SCALE;
        const static float BENCH_SCALE;
        const static float LAMP_SCALE;

        // variable
        cocos2d::Node* _mainNode;
        cocos2d::Node* _roadNode;

        // function
        cocos2d::Node* _createMainNode();
        cocos2d::Node* _createRoadNode();
        void _createRoadBg(cocos2d::Vec2 pos);
        void _createTree(cocos2d::Vec2 pos);
        void _createLamp(cocos2d::Vec2 pos);
        void _createBench(cocos2d::Vec2 pos);
};

#endif // __BACKGROUND_H__