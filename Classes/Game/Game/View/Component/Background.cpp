#include "Common/Config.h"
#include "Game/Game/View/Component/Background.h"

USING_NS_CC;

const std::string Background::ROAD_BG_FILE_PATH = "bg/road_bg.png";
const std::string Background::TREE_FILE_PATH = "decorate/tree.png";
const std::string Background::BENCH_FILE_PATH = "decorate/bench.png";
const std::string Background::LAMP_FILE_PATH = "decorate/lamp.png";
const float Background::TREE_SCALE = 1;
const float Background::BENCH_SCALE = 1;
const float Background::LAMP_SCALE = 1.15;

/* ---------------------------------------------  Creator  --------------------------------------------- */

bool Background::init(){
    _mainNode = this->_createMainNode();
    this->_createRoadBg(Vec2(0, 0));
    this->_createTree(Vec2(351.575, 236.856));
    this->_createTree(Vec2(-354.027, 235.493));
    this->_createTree(Vec2(-465.265, 278.344));
    this->_createTree(Vec2(-534.646, 235.855));
    this->_createTree(Vec2(497.701, 235.574));
    this->_createLamp(Vec2(-315, 222));
    this->_createLamp(Vec2(315, 222));
    _roadNode = this->_createRoadNode();
    this->_createBench(Vec2(-435.004, -235.727));
    this->_createBench(Vec2(445.575, -250.034));
    this->_createLamp(Vec2(-315, -75));
    this->_createLamp(Vec2(315, -75));

    return true;
};

/* ---------------------------------------------  Public  --------------------------------------------- */

Node* Background::getRoadNode(){
    return _roadNode;
};

/* ---------------------------------------------  Private  --------------------------------------------- */


Node* Background::_createMainNode(){
    Node* mainNode = Node::create();
    mainNode->setPosition(Config::CENTER);
    this->addChild(mainNode);
    return mainNode;
};

void Background::_createRoadBg(Vec2 pos){
    Sprite* roadBg = Sprite::create(Background::ROAD_BG_FILE_PATH);
    roadBg->setPosition(pos);
    _mainNode->addChild(roadBg);
};

void Background::_createTree(Vec2 pos){
    Sprite* tree = Sprite::create(Background::TREE_FILE_PATH);
    tree->setPosition(pos);
    tree->setScale(Background::TREE_SCALE);
    _mainNode->addChild(tree);
};

void Background::_createBench(Vec2 pos){
    Sprite* bench = Sprite::create(Background::BENCH_FILE_PATH);
    bench->setPosition(pos);
    bench->setScale(Background::BENCH_SCALE);
    _mainNode->addChild(bench);
};

void Background::_createLamp(Vec2 pos){
    Sprite* lamp = Sprite::create(Background::LAMP_FILE_PATH);
    lamp->setPosition(pos);
    lamp->setScale(Background::LAMP_SCALE);
    _mainNode->addChild(lamp);
};

Node* Background::_createRoadNode(){
    Node* node = Node::create();
    node->setPosition(_mainNode->convertToNodeSpace(Vec2(0, 0)));
    _mainNode->addChild(node);
    return node;
};