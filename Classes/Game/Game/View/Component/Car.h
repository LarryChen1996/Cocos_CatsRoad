#ifndef __CAR_H__
#define __CAR_H__

#include "cocos2d.h"

typedef std::string CarType;
typedef struct {
    float x;
    float y;
    float width;
    float height;
} CarRectDiff;

class Car : public cocos2d::Node{
    public:
        // class const variable 
        const static std::map<std::string, CarType> CAR_TYPE;

        // creator
        static Car* create(CarType carType, bool isFaceRight);
        Car(CarType carType, bool isFaceRight);

        // function
        cocos2d::Rect getCollisionRect();
        void setBorderX(float min, float max);
        void update(float dt);
        void setOutOfScreenCallback(std::function<void(Car*)> callback);

    private: 
        // class const variable 
        const static std::map<CarType, std::string> CAR_FILE_PATH;
        const static std::map<CarType, CarRectDiff> COLLISION_RECT_DIFF;
        const static float MOVE_SPEED;
        const static float DEFAULT_MIN_BORDER_X;
        const static float DEFAULT_MAX_BORDER_X;

        // variable
        CarType _carType;
        bool _isFaceRight;
        float _minBorderX;
        float _maxBorderX;
        std::function<void(Car*)> _outOfScreenCallback;
        cocos2d::Sprite* _carSprite;

        // function
        cocos2d::Sprite* _createCarSprite(CarType carType, bool isFaceRight);
};

#endif // __CAR_H__