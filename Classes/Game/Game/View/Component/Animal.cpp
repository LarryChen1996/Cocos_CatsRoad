#include "Game/Game/View/Component/Animal.h"
#include "Game/Game/GameConfig.h"
#include "Common/Tools.h"
#include "editor-support/cocostudio/SimpleAudioEngine.h"

USING_NS_CC;

/* ---------------- Public Const ---------------- */

const std::map<std::string, AnimalState> Animal::STATE = {
    {"WALK", "WALK"},
    {"RUN", "RUN"},
    {"IDLE", "IDLE"},
    {"DIE", "DIE"},
};

/* ---------------- Private Const ---------------- */

const std::map<std::string, int> Animal::ZORDER = {
    {"IDLE_TIME_BAR"        , 5},
    {"IDLE_SIGN"            , 4},
    {"ANIMAL_FACE_FORWARD"  , 3},
    {"AFTER_GLOW"           , 2},
    {"ANIMAL_FACE_BACKWARD" , 1},
    {"CHOOSED_FRAME"        , 0},
};
const std::string Animal::CHOOSED_FRAME_FILE_PATH          = "animal/animal_choose_circle.png";
const std::string Animal::IDLE_SIGN_FILE_PATH              = "question_mark.png";
const std::string Animal::IDLE_PROGRESS_TIMER_FILE_PATH    = "bar/idle_time_bar.png";
const std::string Animal::AFTER_GLOW_FILE_PATH             = "animal/animal_afterglow.png";
const std::string Animal::EXPLOSION_CIRCLE_FILE_PAHT       = "animal/animal_explosion_circle.png"; 
const Vec2 Animal::IDLE_SIGN_POS_OFFSET                    = Vec2(28, 35);
const Vec2 Animal::IDLE_PROGRESS_TIMER_POS_OFFSET          = Vec2(0, 10);
const Vec2 Animal::AFTER_GLOW_POS_OFFSET                   = Vec2(0, -18);
const AnimalRectDiff Animal::COLLISION_RECT_DIFF           = {0, -10, 0, -10};
const float Animal::AFTER_GLOW_DECREASE_SCALE_PER_SECOND   = 0.9;
const float Animal::AFTER_GLOW_DECREASE_OPACITY_PER_SECOND = 400;
const float Animal::AFTER_GLOW_CREATE_TIME                 = 0.1;
const float Animal::MAX_IDLE_TIME                          = 12;
const float Animal::START_SHOW_IDLE_EFFECT_TIME            = 5;
const float Animal::MAX_PROGRESS_TIMER_PERCENTAGE          = 100;
const float Animal::DEFAULT_MAX_BORDER_Y                   = 640;
const float Animal::DEFAULT_MIN_BORDER_Y                   = 0;
const float Animal::TRIGGER_RUN_TOUCH_DELTA_DISTANCE       = 25;

/* ---------------------------------------------  Creator  --------------------------------------------- */

Animal* Animal::create(AnimalType animalType, bool isFaceForward){
    Animal* instance = new(std::nothrow) Animal(animalType, isFaceForward);

    if (instance){ 
        instance->autorelease();
        return instance;
    }else{
        delete instance;
        instance = nullptr;
        return nullptr;
    };
};

Animal::Animal(AnimalType animalType, bool isFaceForward){
    _animalType          = animalType;
    _isFaceForward       = isFaceForward;
    _animalDetail        = GameConfig::getAnimalDetail(animalType);
    _afterglowCreateTime = Animal::AFTER_GLOW_CREATE_TIME;
    _idleTime            = Animal::MAX_IDLE_TIME;
    _minBorderY          = Animal::DEFAULT_MIN_BORDER_Y;
    _maxBorderY          = Animal::DEFAULT_MAX_BORDER_Y;
    _isIdleEffectPlayed  = false;
    _afterglows          = {};
    _afterglowsPos       = {};

    _stateMachine      = this->_createStateMachine();
    _animalSprite      = this->_createAnimalSprite(animalType, isFaceForward);
    _choosedFrame      = this->_createChoosedFrame();
    _idleSign          = this->_createIdleSign();
    _idleProgressTimer = this->_createIdleProgressTimer();
    _touchListener     = this->_registerScriptTouch();

    this->_setupToIdleFrame();
    this->_resetComponentPosition(_animalSprite->getContentSize());
};

/* ---------------------------------------------  Public  --------------------------------------------- */

void Animal::changeState(AnimalState state){
    if (_stateMachine->isState(state)){
        return;
    };

    _stateMachine->transfer(state);
    _stateMachine->update(0);
};

void Animal::update(float dt){
    _stateMachine->update(dt);
};

AnimalType Animal::getAnimalType(){
    return _animalType;
};

void Animal::setOutOfScreenCallback(std::function<void(Animal*)> callback){
    _outOfScreenCallback = callback;
};

void Animal::setBorderY(float min, float max){
    _minBorderY = min;
    _maxBorderY = max;
};

Rect Animal::getCollisionRect(){
    AnimalRectDiff rectDiff = Animal::COLLISION_RECT_DIFF;
    Rect animalRect = _animalSprite->getBoundingBox();
    Size animalSize = animalRect.size;
    Vec2 animalWorldPos = _animalSprite->convertToWorldSpace(Vec2(0, 0));

    return Rect(
        animalWorldPos.x + rectDiff.x, animalWorldPos.y + rectDiff.y,
        animalSize.width + rectDiff.width, animalSize.height + rectDiff.height
    );
};

Rect Animal::getTouchRect(){
    Rect animalRect = _animalSprite->getBoundingBox();
    Size animalSize = animalRect.size;
    Vec2 animalWorldPos = _animalSprite->convertToWorldSpace(Vec2(0, 0));

    return Rect(
        animalWorldPos.x, animalWorldPos.y,
        animalSize.width, animalSize.height
    );
};

/* ---------------------------------------------  Private  --------------------------------------------- */

/* ------------------------------- */
/* -------- Touch Function ------- */
/* ------------------------------- */

bool Animal::_onTouchBegan(Touch* pTouch, Event* pEvent){
    if (!this->getTouchRect().containsPoint(pTouch->getLocation())){
        return false;
    }else{
        _choosedFrame->setVisible(true);
        return true;
    };
};

void Animal::_onTouchMoved(Touch* pTouch, Event* pEvent){
    if (pTouch->getDelta().getDistance(Vec2(0, 0)) >= Animal::TRIGGER_RUN_TOUCH_DELTA_DISTANCE){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/run.mp3");
        this->changeState(Animal::STATE.at("RUN"));
    };
};

void Animal::_onTouchEnded(Touch* pTouch, Event* pEvent){
    _choosedFrame->setVisible(false);
    
    if (this->getTouchRect().containsPoint(pTouch->getLocation())){
        this->changeState(
            _stateMachine->isState(Animal::STATE.at("IDLE")) ? Animal::STATE.at("WALK") 
                                                             : Animal::STATE.at("IDLE")
        );
    };
};

/* ------------------------------- */
/* -------- State Function ------- */
/* ------------------------------- */

void Animal::_onWalkState(bool isFirstExec, float dt){
    if (isFirstExec){
        _idleSign->setVisible(false);
        _choosedFrame->setVisible(false);
        _idleProgressTimer->setVisible(false);
        _animalSprite->stopAllActions();
        _animalSprite->runAction(this->_getStateAnimation(Animal::STATE.at("WALK")));
    };

    this->_refreshPosition(_animalDetail.WALK_SPEED, dt);
};

void Animal::_onRunState(bool isFirstExec, float dt){
    if (isFirstExec){
        _afterglowCreateTime = Animal::AFTER_GLOW_CREATE_TIME;
        _touchListener->setEnabled(false);
        _idleSign->setVisible(false);
        _choosedFrame->setVisible(false);
        _idleProgressTimer->setVisible(false);
        _animalSprite->stopAllActions();
        _animalSprite->runAction(this->_getStateAnimation(Animal::STATE.at("RUN")));
    };  

    this->_createAfterglow(dt);
    this->_updateAfterglows(dt);
    this->_refreshPosition(_animalDetail.RUN_SPEED, dt);
};

void Animal::_onIdleState(bool isFirstExec, float dt){
    if (isFirstExec){
        _idleTime = Animal::MAX_IDLE_TIME;
        _isIdleEffectPlayed = false;
        this->_setupToIdleFrame();
    };
    
    _idleTime = _idleTime - dt;
    this->_updateIdleStatus();
    this->_updateIdleProgressTimer();
};

void Animal::_onDieState(bool isFirstExec, float dt){
    if (isFirstExec){
        for (int index = 0; index < _afterglows.size(); index++){
            _afterglows.at(index)->removeFromParentAndCleanup(true);
        };
        _afterglows.clear();
        _afterglowsPos.clear(); 
        _touchListener->setEnabled(false);
        _animalSprite->setVisible(false);
        _choosedFrame->setVisible(false);
        _idleSign->setVisible(false);
        _idleProgressTimer->setVisible(false);
        this->_playExplosionCircleAnimation();
    };
};

/* ----------------------------- */
/* ----------- Other ----------- */
/* ----------------------------- */

void Animal::_playExplosionCircleAnimation(){
    Sprite* explosionCircle = Sprite::create(Animal::EXPLOSION_CIRCLE_FILE_PAHT);
    explosionCircle->setScale(0);
    explosionCircle->setOpacity(0);
    this->addChild(explosionCircle);

    explosionCircle->runAction(Sequence::create(
        Spawn::create(
            EaseSineIn::create(ScaleTo::create(0.3, 1)),
            EaseSineIn::create(FadeIn::create(0.3)),
            nullptr
        ),
        DelayTime::create(0.1),
        Spawn::create(
            EaseSineIn::create(ScaleTo::create(0.3, 0)),
            FadeOut::create(0.3),
            nullptr
        ),
        CallFunc::create([this](){
            this->removeFromParentAndCleanup(true);
        }),
        nullptr
    ));
};

void Animal::_updateIdleStatus(){
    if (_idleTime <= 0){
        this->changeState(Animal::STATE.at("WALK"));
    }else if (_idleTime <= Animal::START_SHOW_IDLE_EFFECT_TIME & !_isIdleEffectPlayed){
        _isIdleEffectPlayed = true;
        _idleSign->setVisible(true);
        _idleProgressTimer->setVisible(true);
        _idleProgressTimer->setPercentage(Animal::MAX_PROGRESS_TIMER_PERCENTAGE);
    };
};

void Animal::_updateIdleProgressTimer(){
    if (!_isIdleEffectPlayed){
        return;
    };

    float percentage = (_idleTime / Animal::START_SHOW_IDLE_EFFECT_TIME) * Animal::MAX_PROGRESS_TIMER_PERCENTAGE;
    _idleProgressTimer->setPercentage(percentage);
};

void Animal::_createAfterglow(float dt){
    _afterglowCreateTime = _afterglowCreateTime - dt;
    if (_afterglowCreateTime > 0){
        return;
    };

    Sprite* afterglow = Sprite::create(Animal::AFTER_GLOW_FILE_PATH);
    this->addChild(afterglow);
    _afterglowsPos.push_back(this->getPosition());
    _afterglows.push_back(afterglow);
    _afterglowCreateTime = Animal::AFTER_GLOW_CREATE_TIME;
};

void Animal::_updateAfterglows(float dt){
    if (_afterglows.empty()){
        return;
    };

    float scaleDecreaseStep = Animal::AFTER_GLOW_DECREASE_SCALE_PER_SECOND * dt;
    float opacityDecreaseStep = Animal::AFTER_GLOW_DECREASE_OPACITY_PER_SECOND * dt;

    for (int index = _afterglows.size() - 1; index >= 0; index--){
        Sprite* afterglow = _afterglows.at(index);
        float finalScale = afterglow->getScale() - scaleDecreaseStep;
        float finalOpacity = afterglow->getOpacity() - opacityDecreaseStep;

        if (finalScale <= 0 | finalOpacity <= 0){
            this->removeChild(afterglow, true);
            _afterglows.erase(_afterglows.begin() + index);
            _afterglowsPos.erase(_afterglowsPos.begin() + index);
        }else{
            Vec2 createPos = _afterglowsPos.at(index);
            afterglow->setPosition(this->convertToNodeSpace(createPos));
            afterglow->setScale(finalScale);
            afterglow->setOpacity(finalOpacity);
        };
    };
};

void Animal::_setupToIdleFrame(){
    AnimalAnimationDetail animationDetail = _isFaceForward ? _animalDetail.FORWARD_ANIMATION_DETAIL
                                                           : _animalDetail.BACKWWARD_ANIMATION_DETAIL;
    _animalSprite->stopAllActions();
    _animalSprite->setSpriteFrame(Tools::formatString(animationDetail.FILE_NAME, animationDetail.IDLE_FRAME));
};

void Animal::_refreshPosition(float speed, float dt){
    float displacementY = speed * dt;
    displacementY = _isFaceForward ? displacementY : - displacementY;
    Vec2 originPos = this->getPosition();
    Vec2 finalPos = Vec2(originPos.x, originPos.y + displacementY);
    this->setPosition(finalPos);

    if ((finalPos.y >= _maxBorderY & _isFaceForward) | (finalPos.y <= _minBorderY & !_isFaceForward)){
        if (_outOfScreenCallback != nullptr){
            _outOfScreenCallback(this);
        };

        this->changeState(Animal::STATE.at("DIE"));
    };
};

FiniteTimeAction* Animal::_getStateAnimation(AnimalState state){
    bool isWalkState = state == Animal::STATE.at("WALK");
    bool isRunState = state == Animal::STATE.at("RUN");
    if (!isWalkState & !isRunState){
        return;
    };

    AnimalAnimationDetail animationDetail = _isFaceForward ? _animalDetail.FORWARD_ANIMATION_DETAIL
                                                           : _animalDetail.BACKWWARD_ANIMATION_DETAIL;
    auto animation = Animation::createWithSpriteFrames(
        Tools::getAnimationFrames(animationDetail.FILE_NAME, 1, animationDetail.TOTAL_FRAMES),
        isWalkState ? animationDetail.WALK_RATE : animationDetail.RUN_RATE
    );
    return RepeatForever::create(Animate::create(animation));
};

/* ----------------------------- */
/* ---------- Create ----------- */
/* ----------------------------- */

StateMachine* Animal::_createStateMachine(){
    StateMachine* stateMachine = StateMachine::create("", this);
    stateMachine->addState(Animal::STATE.at("WALK"), CC_CALLBACK_2(Animal::_onWalkState, this));
    stateMachine->addState(Animal::STATE.at("RUN"), CC_CALLBACK_2(Animal::_onRunState, this));
    stateMachine->addState(Animal::STATE.at("IDLE"), CC_CALLBACK_2(Animal::_onIdleState, this));
    stateMachine->addState(Animal::STATE.at("DIE"), CC_CALLBACK_2(Animal::_onDieState, this));
    return stateMachine;
};

Sprite* Animal::_createAnimalSprite(AnimalType animalType, bool isFaceForward){
    int zorder = Animal::ZORDER.at(isFaceForward ? "ANIMAL_FACE_FORWARD" : "ANIMAL_FACE_BACKWARD");
    Sprite* animalSprite = Sprite::create();
    this->addChild(animalSprite, zorder);
    return animalSprite;
};

Sprite* Animal::_createChoosedFrame(){
    Sprite* choosedFrame = Sprite::create(Animal::CHOOSED_FRAME_FILE_PATH);
    choosedFrame->setVisible(false);
    choosedFrame:setColor(Color3B(255, 0, 0));
    this->addChild(choosedFrame, Animal::ZORDER.at("CHOOSED_FRAME"));
    return choosedFrame;
};

Sprite* Animal::_createIdleSign(){
    Sprite* idleSign = Sprite::create(Animal::IDLE_SIGN_FILE_PATH);
    idleSign->setVisible(false);
    this->addChild(idleSign, Animal::ZORDER.at("IDLE_SIGN"));
    return idleSign;
};

ProgressTimer* Animal::_createIdleProgressTimer(){
    ProgressTimer* idleProgressTimer = ProgressTimer::create(Sprite::create(Animal::IDLE_PROGRESS_TIMER_FILE_PATH));
    idleProgressTimer->setType(ProgressTimer::Type::BAR);
    idleProgressTimer->setMidpoint(Vec2(0, 0.5));
    idleProgressTimer->setBarChangeRate(Vec2(1, 0));
    idleProgressTimer->setPercentage(100);
    idleProgressTimer->setVisible(false);
    this->addChild(idleProgressTimer, Animal::ZORDER.at("IDLE_TIME_BAR"));
    return idleProgressTimer;
};

EventListenerTouchOneByOne* Animal::_registerScriptTouch(){
    EventListenerTouchOneByOne* touchListener = EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(Animal::_onTouchBegan, this);
    touchListener->onTouchMoved = CC_CALLBACK_2(Animal::_onTouchMoved, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(Animal::_onTouchEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
    return touchListener;
};

void Animal::_resetComponentPosition(Size animalSpriteSize){
    _idleSign->setPosition(
        Animal::IDLE_SIGN_POS_OFFSET.x,
        animalSpriteSize.height / 2 + Animal::IDLE_SIGN_POS_OFFSET.y
    );
    _idleProgressTimer->setPosition(
        Animal::IDLE_PROGRESS_TIMER_POS_OFFSET.x,
        animalSpriteSize.height / 2 + Animal::IDLE_PROGRESS_TIMER_POS_OFFSET.y
    );
};