#include "Car.h"

USING_NS_CC;

/* ---------------- Public Const ---------------- */

const std::map<std::string, CarType> Car::CAR_TYPE = {
    {"TYPE_1", "TYPE_1"},
    {"TYPE_2", "TYPE_2"},
    {"TYPE_3", "TYPE_3"},
    {"TYPE_4", "TYPE_4"},
    {"TYPE_5", "TYPE_5"},
    {"TYPE_6", "TYPE_6"},
    {"TYPE_7", "TYPE_7"},
    {"TYPE_8", "TYPE_8"},
    {"TYPE_9", "TYPE_9"},
};

/* ---------------- Private Const ---------------- */

const std::map<CarType, std::string> Car::CAR_FILE_PATH = {
    {Car::CAR_TYPE.at("TYPE_1"), "car/car_1.png"},
    {Car::CAR_TYPE.at("TYPE_2"), "car/car_2.png"},
    {Car::CAR_TYPE.at("TYPE_3"), "car/car_3.png"},
    {Car::CAR_TYPE.at("TYPE_4"), "car/car_4.png"},
    {Car::CAR_TYPE.at("TYPE_5"), "car/car_5.png"},
    {Car::CAR_TYPE.at("TYPE_6"), "car/car_6.png"},
    {Car::CAR_TYPE.at("TYPE_7"), "car/car_7.png"},
    {Car::CAR_TYPE.at("TYPE_8"), "car/car_8.png"},
    {Car::CAR_TYPE.at("TYPE_9"), "car/car_9.png"},
};
const std::map<CarType, CarRectDiff> Car::COLLISION_RECT_DIFF = {
    {Car::CAR_TYPE.at("TYPE_1"), CarRectDiff{0, 5, 0, -30}},
    {Car::CAR_TYPE.at("TYPE_2"), CarRectDiff{0, 5, 0, -40}},
    {Car::CAR_TYPE.at("TYPE_3"), CarRectDiff{0, 5, 0, -10}},
    {Car::CAR_TYPE.at("TYPE_4"), CarRectDiff{0, 5, 0, -20}},
    {Car::CAR_TYPE.at("TYPE_5"), CarRectDiff{0, 5, 0, -5}},
    {Car::CAR_TYPE.at("TYPE_6"), CarRectDiff{0, 5, 0, -10}},
    {Car::CAR_TYPE.at("TYPE_7"), CarRectDiff{0, 5, 0, -10}},
    {Car::CAR_TYPE.at("TYPE_8"), CarRectDiff{0, 5, 0, -30}},
    {Car::CAR_TYPE.at("TYPE_9"), CarRectDiff{0, 5, 0, -5}},
};
const float Car::MOVE_SPEED           = 150;
const float Car::DEFAULT_MIN_BORDER_X = 0;
const float Car::DEFAULT_MAX_BORDER_X = 1136;

/* ---------------------------------------------  Creator  --------------------------------------------- */

Car* Car::create(CarType carType, bool isFaceRight){
    Car* instance = new(std::nothrow) Car(carType, isFaceRight);

    if (instance){ 
        instance->autorelease();
        return instance;
    }else{
        delete instance;
        instance = nullptr;
        return nullptr;
    };
};

Car::Car(CarType carType, bool isFaceRight){
    _carType = carType;
    _isFaceRight = isFaceRight;
    _minBorderX = Car::DEFAULT_MIN_BORDER_X;
    _maxBorderX = Car::DEFAULT_MAX_BORDER_X;
    _carSprite = this->_createCarSprite(carType, isFaceRight);
};

/* ---------------------------------------------  Public  --------------------------------------------- */

Rect Car::getCollisionRect(){
    CarRectDiff rectDiff = Car::COLLISION_RECT_DIFF.at(_carType);
    Rect carRect = _carSprite->getBoundingBox();
    Size carSize = carRect.size;
    Vec2 carWorldPos = _carSprite->convertToWorldSpace(Vec2(0, 0));

    return Rect(
        carWorldPos.x + rectDiff.x, carWorldPos.y + rectDiff.y,
        carSize.width + rectDiff.width, carSize.height + rectDiff.height
    );
};

void Car::setBorderX(float min, float max){
    _minBorderX = min;
    _maxBorderX = max;
};

void Car::update(float dt){
    float displacementX = Car::MOVE_SPEED * dt;
    displacementX = _isFaceRight ? displacementX : - displacementX;
    Vec2 originPos = this->getPosition();
    Vec2 finalPos = Vec2(originPos.x + displacementX, originPos.y);
    this->setPosition(finalPos);

    if ((finalPos.x >= _maxBorderX & _isFaceRight) | (finalPos.x <= _minBorderX & !_isFaceRight)){
        _outOfScreenCallback(this);
        this->removeFromParentAndCleanup(true);
    };
};

void Car::setOutOfScreenCallback(std::function<void(Car*)> callback){
    _outOfScreenCallback = callback;  
};

/* ---------------------------------------------  Private  --------------------------------------------- */

Sprite* Car::_createCarSprite(CarType carType, bool isFaceRight){
    Sprite* carSprite = Sprite::create(Car::CAR_FILE_PATH.at(carType.c_str()));
    carSprite->setFlippedX(isFaceRight);
    carSprite->setAnchorPoint(Vec2(0.5, 0));
    this->addChild(carSprite);
    return carSprite;
};
