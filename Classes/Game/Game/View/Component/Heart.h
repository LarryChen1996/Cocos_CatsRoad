#ifndef __HEART_H__
#define __HEART_H__

#include "cocos2d.h"

class Heart : public cocos2d::Node{
    public:
        // creator
        CREATE_FUNC(Heart);
        virtual bool init();

        // function
        void update(float dt);
        void setDisappearCallback(std::function<void(Heart*)> callback);
        cocos2d::Rect getCollisionRect();
    
    private:
        // class const vairable
        const static std::string HEART_SPRITE_FILE_PATH;
        const static float EXIST_REMAINING_TIME;
        const static float START_FADE_EFFECT_TIME;

        // variable
        std::function<void(Heart*)> _disappearCallback;
        cocos2d::Sprite* _heartSprite;
        float _existRemainingTime;
        bool _fadeEffectPlayed;

        // function
        cocos2d::Sprite* _createHeartSprite();
        void _disappear();
        void _playFadeEffect();
};

#endif // __HEART_H__