#include "Game/Game/View/Component/Heart.h"

USING_NS_CC;

const std::string Heart::HEART_SPRITE_FILE_PATH = "heart/heart.png";
const float Heart::EXIST_REMAINING_TIME         = 12;
const float Heart::START_FADE_EFFECT_TIME       = 5;

bool Heart::init(){
    _existRemainingTime = Heart::EXIST_REMAINING_TIME;
    _fadeEffectPlayed = false;

    _heartSprite = _createHeartSprite();

    return true;
};

void Heart::update(float dt){
    if (_existRemainingTime <= 0){
        return;
    };

    _existRemainingTime = _existRemainingTime - dt;
    if (_existRemainingTime <= 0){
        this->_disappear();
    }else if (_existRemainingTime <= Heart::START_FADE_EFFECT_TIME && _fadeEffectPlayed == false){
        this->_playFadeEffect();
    };
};

void Heart::setDisappearCallback(std::function<void(Heart*)> callback){
    _disappearCallback = callback;
};

Rect Heart::getCollisionRect(){
    Rect heartRect = _heartSprite->getBoundingBox();
    Size heartSize = heartRect.size;
    Vec2 heartWorldPos = _heartSprite->convertToWorldSpace(Vec2(0, 0));

    return Rect(
        heartWorldPos.x, heartWorldPos.y,
        heartSize.width, heartSize.height
    );
};

Sprite* Heart::_createHeartSprite(){
    Sprite* heartSprite = Sprite::create(Heart::HEART_SPRITE_FILE_PATH);
    this->addChild(heartSprite);
    return heartSprite;
};

void Heart::_disappear(){
    _heartSprite->stopAllActions();
    _heartSprite->setOpacity(255);
    _heartSprite->runAction(Sequence::create(
        ScaleTo::create(0.1, 1.4),
        ScaleTo::create(0.18, 0),
        CallFunc::create(
            [this](){ 
                this->removeFromParentAndCleanup(true); 
            }
        ),
        nullptr
    ));

    if (_disappearCallback != nullptr){
        _disappearCallback(this);
    };
};

void Heart::_playFadeEffect(){
    _fadeEffectPlayed = true;
    _heartSprite->runAction(RepeatForever::create(Sequence::create(
        FadeTo::create(0.2, 120),
        FadeTo::create(0.2, 255),
        nullptr
    )));
};
