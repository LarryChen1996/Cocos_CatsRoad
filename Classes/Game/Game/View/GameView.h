#ifndef __GAME_VIEW_H__
#define __GAME_VIEW_H__

#include "cocos2d.h"
#include "Game/Game/GameConfig.h"
#include "Game/Game/View/GameView.h"
#include "Game/Game/View/Component/Background.h"
#include "Game/Game/View/Component/Car.h"
#include "Game/Game/View/Component/Animal.h"
#include "Game/Game/View/Component/Heart.h"
#include "editor-support/cocostudio/SimpleAudioEngine.h"

typedef struct {
    float MIN_X;
    float MAX_X;
    float MIN_Y;
    float MAX_Y;
} GameViewObjectPosRestrict;

typedef std::string GameViewEvent;
typedef std::function<void(GameViewEvent, void*)> GameViewEventListener;

class GameView : public cocos2d::Layer{
    public:
        // class const variable
        const static std::map<std::string, GameViewEvent> EVENT;

        // creator
        CREATE_FUNC(GameView);
        virtual bool init() override;

        // function
        void createAnimal(AnimalType animalType);
        void createRandomCar();
        void createRandomHeartOnRoad();
        virtual void update(float dt) override;
        void removeAllAnimals();
        void setEventListener(GameViewEventListener eventListener);

    private: 
        // TODO 看看有沒有辦法不用 function init 變數
        // class const variable
        const static std::map<std::string, int> ZORDER;
    
        GameViewObjectPosRestrict ANIMAL_POS_RESTRICT;
        GameViewObjectPosRestrict CAR_POS_RESTRICT;
        GameViewObjectPosRestrict HEART_POS_RESTRICT;
        void _initVariableWithConfigData();
    
        
        // function
        void _updateAnimals(float dt);
        void _updateCars(float dt);
        void _updateHearts(float dt);
        void _checkCollisionBetweenAnimalAndCar();
        void _checkCollisionBetweenAnimalAndHeart();
        void _onAnimalOutOfScreen(Animal* targetAnimal);
        void _onCarOutOfScreen(Car* targetCar);
        void _onHeartDisappear(Heart* targetHeart);
        void _onAnimalCollideCar(int animalIndex);
        void _onAnimalCollideHeart(int heartIndex);
        void _dispatchEvent(GameViewEvent event, void* customData = nullptr);

        cocos2d::Node* _createBackground();

        // variable
        cocos2d::Node* _roadNode;
        Heart* _heart;
        std::vector<Car*> _cars;
        std::vector<Animal*> _animals;
        std::vector<Heart*> _hearts;
        GameViewEventListener _eventListener;
        CocosDenshion::SimpleAudioEngine* _audioEngine;
};

#endif // __GAME_VIEW_H__