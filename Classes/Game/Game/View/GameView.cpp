#include "Game/Game/View/GameView.h"
#include "Common/Config.h"
#include "Common/Tools.h"
#include "Game/Game/GameConfig.h"
#include "Game/Game/View/Component/Car.h"
#include "Game/Game/View/Component/Animal.h"
#include "Game/Game/View/Component/Heart.h"
#include "editor-support/cocostudio/SimpleAudioEngine.h"

USING_NS_CC;

/* ---------------- Public Const ---------------- */

const std::map<std::string, GameViewEvent> GameView::EVENT = {
    {"ANIMAL_OUT_OF_SCREEN", "ANIMAL_OUT_OF_SCREEN"},
    {"CAR_OUT_OF_SCREEN", "CAR_OUT_OF_SCREEN"},
    {"HEART_DISAPPEAR", "HEART_DISAPPEAR"},
    {"ANIMAL_COLLIDE_CAR", "ANIMAL_COLLIDE_CAR"},
    {"ANIMAL_COLLIDE_HEART", "ANIMAL_COLLIDE_HEART"},
};

/* ---------------- Private Const ---------------- */

const std::map<std::string, int> GameView::ZORDER = {
    {"CAR_ABOVE", 4},
    {"CAR_BOTTOM", 3},
    {"ANIMAL", 2},
    {"HEART", 1},
};

/* ---------------------------------------------  Creator  --------------------------------------------- */

bool GameView::init(){
    this->_initVariableWithConfigData();

    _roadNode = this->_createBackground();
    _audioEngine = CocosDenshion::SimpleAudioEngine::getInstance();

    return true;
};

void GameView::_initVariableWithConfigData(){
    ANIMAL_POS_RESTRICT = {
        static_cast<float>(Config::CENTER_X) - 250,
        static_cast<float>(Config::CENTER_X) + 250,
        -80,
        static_cast<float>(Config::WIN_SIZE.height) + 80,
    };
    CAR_POS_RESTRICT = {
        -200,
        static_cast<float>(Config::WIN_SIZE.width) + 200,
        static_cast<float>(Config::CENTER_Y) - 95,
        static_cast<float>(Config::CENTER_Y) + 30,
    };
    HEART_POS_RESTRICT = {
        static_cast<float>(Config::CENTER_X) - 250,
        static_cast<float>(Config::CENTER_X) + 250,
        static_cast<float>(Config::CENTER_Y) + 60,
        static_cast<float>(Config::CENTER_Y) - 60,
    };
};

/* ---------------------------------------------  Public  --------------------------------------------- */

void GameView::createAnimal(AnimalType animalType){
    bool isCreateOnTop = Tools::randomNumber<int>(1, 2) == 1;
    bool isFaceForward = !isCreateOnTop;
    int zorder = GameView::ZORDER.at("ANIMAL");
    Vec2 pos = Vec2(
        Tools::randomNumber<float>(ANIMAL_POS_RESTRICT.MIN_X, ANIMAL_POS_RESTRICT.MAX_X),
        isCreateOnTop ? ANIMAL_POS_RESTRICT.MAX_Y : ANIMAL_POS_RESTRICT.MIN_Y
    );

    Animal* animal = Animal::create(animalType, isFaceForward);
    animal->setOutOfScreenCallback(CC_CALLBACK_1(GameView::_onAnimalOutOfScreen, this));
    animal->setPosition(pos);
    animal->changeState(Animal::STATE.at("WALK"));
    _roadNode->addChild(animal, zorder);
    _animals.push_back(animal);
};

void GameView::createRandomCar(){
    bool isCreateOnAbove = Tools::randomNumber<int>(1, 2) == 1;
    bool isFaceRight = isCreateOnAbove;
    int zorder = GameView::ZORDER.at(isCreateOnAbove ? "CAR_ABOVE" : "CAR_BOTTOM");
    CarType carType = Tools::getMapRandomValue<std::string, CarType>(Car::CAR_TYPE);
    Vec2 pos = Vec2(
        isFaceRight ? CAR_POS_RESTRICT.MIN_X : CAR_POS_RESTRICT.MAX_X,
        isCreateOnAbove ? CAR_POS_RESTRICT.MAX_Y : CAR_POS_RESTRICT.MIN_Y
    );

    Car* car = Car::create(carType, isFaceRight);
    car->setOutOfScreenCallback(CC_CALLBACK_1(GameView::_onCarOutOfScreen, this));
    car->setBorderX(CAR_POS_RESTRICT.MIN_X, CAR_POS_RESTRICT.MAX_X);
    car->setPosition(pos);
    _roadNode->addChild(car, zorder);
    _cars.push_back(car);
    _audioEngine->playEffect("audio/car.mp3");
};

void GameView::createRandomHeartOnRoad(){
    int zorder = GameView::ZORDER.at("HEART");
    Vec2 pos = Vec2(
        Tools::randomNumber<float>(HEART_POS_RESTRICT.MIN_X, HEART_POS_RESTRICT.MAX_X),
        Tools::randomNumber<float>(HEART_POS_RESTRICT.MIN_Y, HEART_POS_RESTRICT.MAX_Y)
    );
    
    Heart* heart = Heart::create();
    heart->setDisappearCallback(CC_CALLBACK_1(GameView::_onHeartDisappear, this));
    heart->setPosition(pos);
    _roadNode->addChild(heart, zorder);
    _hearts.push_back(heart);
};

void GameView::update(float dt){
    this->_updateAnimals(dt);
    this->_updateCars(dt);
    this->_updateHearts(dt);
    this->_checkCollisionBetweenAnimalAndCar();
    this->_checkCollisionBetweenAnimalAndHeart();
};

void GameView::removeAllAnimals(){
    for (int index = 0; index < _animals.size(); index++){
        _animals.at(index)->changeState(Animal::STATE.at("DIE"));
    };
    _animals.clear();
};

void GameView::setEventListener(GameViewEventListener eventListener){
    _eventListener = eventListener;
};

/* ---------------------------------------------  Private  --------------------------------------------- */

/* ------------------------------- */
/* ------- Update Function ------- */
/* ------------------------------- */

void GameView::_updateAnimals(float dt){
    for (int index = 0; index < _animals.size(); index++){
        _animals.at(index)->update(dt);
    };
};

void GameView::_updateCars(float dt){
    for (int index = 0; index < _cars.size(); index++){
        _cars.at(index)->update(dt);
    };
};

void GameView::_updateHearts(float dt){
    for (int index = 0; index < _hearts.size(); index++){
        _hearts.at(index)->update(dt);
    };
};

void GameView::_checkCollisionBetweenAnimalAndCar(){
    for (int animalIndex = _animals.size() - 1; animalIndex >= 0; animalIndex--){
        if (_animals.empty()){
            return; // 若結束遊戲清除場上所有動物, 未跳出迴圈將會報錯
        };

        Rect animalRect = _animals.at(animalIndex)->getCollisionRect(); 
        for (int carIndex = _cars.size() - 1; carIndex >= 0; carIndex--){
            Rect carRect = _cars.at(carIndex)->getCollisionRect();
            if (animalRect.intersectsRect(carRect)){
                this->_onAnimalCollideCar(animalIndex);
                break;
            };
        };
    };
};

void GameView::_checkCollisionBetweenAnimalAndHeart(){
    for (int animalIndex = _animals.size() - 1; animalIndex >= 0; animalIndex--){
        Rect animalRect = _animals.at(animalIndex)->getCollisionRect(); 
        for (int heartIndex = _hearts.size() - 1; heartIndex >= 0; heartIndex--){
            Rect heartRect = _hearts.at(heartIndex)->getCollisionRect();
            if (animalRect.intersectsRect(heartRect)){
                this->_onAnimalCollideHeart(heartIndex);
            };
        };
    };
};

/* ------------------------------- */
/* -------- Event Function ------- */
/* ------------------------------- */

void GameView::_onAnimalOutOfScreen(Animal* targetAnimal){
    int animalIndex = Tools::findVectorIndex<Animal*>(_animals, targetAnimal);
    if (animalIndex == -1){
        return;
    };

    AnimalType animalType = targetAnimal->getAnimalType();
    _animals.erase(_animals.begin() + animalIndex);
    this->_dispatchEvent(
        GameView::EVENT.at("ANIMAL_OUT_OF_SCREEN"),
        static_cast<std::string*>(&animalType)
    ); 
    _audioEngine->playEffect("audio/reward_score.mp3");
};

void GameView::_onCarOutOfScreen(Car* targetCar){
    int carIndex = Tools::findVectorIndex<Car*>(_cars, targetCar);
    if (carIndex == Tools::VECTOR_NON_INDEX){
        return;
    };

    _cars.erase(_cars.begin() + carIndex);
    this->_dispatchEvent(GameView::EVENT.at("CAR_OUT_OF_SCREEN")); 
};

void GameView::_onHeartDisappear(Heart* targetHeart){
    int heartIndex = Tools::findVectorIndex<Heart*>(_hearts, targetHeart);
    if (heartIndex == Tools::VECTOR_NON_INDEX){
        return;
    };

    _hearts.erase(_hearts.begin() + heartIndex);
    this->_dispatchEvent(GameView::EVENT.at("HEART_DISAPPEAR")); 
};

void GameView::_onAnimalCollideCar(int animalIndex){
    _animals.at(animalIndex)->changeState(Animal::STATE.at("DIE"));
    _animals.erase(_animals.begin() + animalIndex);
    this->_dispatchEvent(GameView::EVENT.at("ANIMAL_COLLIDE_CAR"));  
    _audioEngine->playEffect("audio/animal_hitted.mp3");
};

void GameView::_onAnimalCollideHeart(int heartIndex){
    _hearts.at(heartIndex)->removeFromParentAndCleanup(true);
    _hearts.erase(_hearts.begin() + heartIndex);
    this->_dispatchEvent(GameView::EVENT.at("ANIMAL_COLLIDE_HEART"));
    _audioEngine->playEffect("audio/get_heart.mp3");
};

/* ------------------------------- */
/* -------- Other Function ------- */
/* ------------------------------- */

void GameView::_dispatchEvent(GameViewEvent event, void* customData){
    if (_eventListener != nullptr){
        _eventListener(event, customData);
    };
};

/* ------------------------------- */
/* ------- Create Function ------- */
/* ------------------------------- */

Node* GameView::_createBackground(){
    Background* background = Background::create();
    this->addChild(background);
    return background->getRoadNode();
};

