#ifndef __GAMEUIVIEW_H__
#define __GAMEUIVIEW_H__

#include "cocos2d.h"
#include "Common/View/ItemBar.h"

class GameUIView : public cocos2d::Layer{
    public:
        // creator
        static GameUIView* create(int maxHealth);
        GameUIView(int maxHealth);

        // function
        void setHealth(int health);
        void setScore(int score);
        void showNewAnimalComing();

    private:
        // class const variable
        const static std::string HEART_FILE_PATH;
        const static std::string HEART_EMPTY_FILE_PATH;
        const static cocos2d::Vec2 HEART_POS_OFFSET;
        const static cocos2d::Vec2 HEART_BAR_POS;
        const static cocos2d::Vec2 SCORE_LABEL_POS;
        const static cocos2d::Color3B SCORE_LABEL_COLOR;
        const static std::string SCORE_LABEL_TEXT;
        const static float NEW_ANIMAL_LAVEL_SCALE;
        const static std::string NEW_ANIMAL_LABEL_TEXT;
        const static std::string RED_SCREEN_BORDER_FILE_PATH;
        const static float RED_SCREEN_BORDER_FADE_DURATION;

        cocos2d::Vec2 NEW_ANIMAL_LABEL_POS;
        cocos2d::Vec2 RED_SCREEN_BORDER_POS;

        // variable
        ItemBar* _heartBar;
        cocos2d::Label* _scoreLabel;
        cocos2d::Label* _newAnimalLabel;
        cocos2d::Sprite* _redScreenBorder;

        // function
        ItemBar* _createHeartBar(int maxHealth);
        cocos2d::Label* _createScoreLabel(int maxHealth);
        cocos2d::Label* _createNewAnimalLabel();
        cocos2d::Sprite* _createRedScreenBorder();
};

#endif // __GAMEUIVIEW_H__