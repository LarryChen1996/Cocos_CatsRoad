#ifndef __GAME_MODEL_H__
#define __GAME_MODEL_H__

#include "functional"
#include "vector"
#include "map"
#include "Game/Game/GameConfig.h"

typedef std::string GameModelEvent;
typedef std::function<void(GameModelEvent, void*)> GameModelEventListener;

class GameModel{
    public:
        // class const variable
        const static std::map<std::string, GameModelEvent> EVENT;

        // creator
        static GameModel* create();
        GameModel();

        // fucntion
        void update(float dt);
        void addScore(AnimalType animalType);
        void addHealth();
        void minusHealth();
        void minusCarsAmount();
        void minusHeartsAmount();
        int getScore();
        int getHealth();
        void setEventListener(GameModelEventListener eventListener);

    private:
        // variable with Config data
        const static float MAX_ANIMAL_CREATE_TIME;
        const static float MIN_ANIMAL_CREATE_TIME;
        const static float MAX_CAR_CREATE_TIME;
        const static float MIN_CAR_CREATE_TIME;
        const static float MAX_HEART_CREATE_TIME;
        const static float MIN_HEART_CREATE_TIME;
        const static int MAX_HEALTH;
        const static int MAX_ON_ROAD_CAR_AMOUNT;
        const static int MAX_ON_ROAD_HEART_AMOUNT;

        // TODO 看看有沒有辦法不用 function init 變數
        std::map<std::string, int> REWARD_SCORE;
        std::map<std::string, int> ANIMAL_AVAILABLE_SCORE;
        void _initVariableWithConfigData();

        // variable
        int _score;
        int _carsAmount;
        int _heartsAmount;
        int _health;
        float _carCreateTimer;
        float _animalCreateTimer;
        float _heartCreateTimer;
        std::vector<AnimalType> _availableAnimalTypesVector;
        GameModelEventListener _eventListener;

        // function
        void _updateCardCreateTimer(float dt);
        void _updateAnimalCreateTimer(float dt);
        void _updateHeartCreateTimer(float dt);
        void _dispatchEvent(GameModelEvent event, void* customData = nullptr);
        void _checkNewAvailableAnimalAppear();
        std::vector<AnimalType> _getAvailableCreateAnimalTypesVector(int socre);
};

#endif // __GAME_MODEL_H__