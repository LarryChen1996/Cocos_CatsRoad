#include "Game/Game/Model/GameModel.h"
#include "Common/Tools.h"
#include "Game/Game/GameConfig.h"

const float GameModel::MAX_ANIMAL_CREATE_TIME = 5;
const float GameModel::MIN_ANIMAL_CREATE_TIME = 3;

const float GameModel::MAX_CAR_CREATE_TIME = 10;
const float GameModel::MIN_CAR_CREATE_TIME = 7;

const float GameModel::MAX_HEART_CREATE_TIME = 18;
const float GameModel::MIN_HEART_CREATE_TIME = 10;

const int GameModel::MAX_HEALTH               = 3;
const int GameModel::MAX_ON_ROAD_CAR_AMOUNT   = 5;
const int GameModel::MAX_ON_ROAD_HEART_AMOUNT = 3;

const std::map<std::string, GameModelEvent> GameModel::EVENT = {
    {"CREATE_CAR", "CREATE_CAR"},
    {"CREATE_ANIMAL", "CREATE_ANIMAL"},
    {"CREATE_HEART", "CREATE_HEART"},
    {"NEW_ANIMAL_APPEAR", "NEW_ANIMAL_APPEAR"},
    {"OUT_OF_HEALTH", "OUT_OF_HEALTH"},
};

/* ---------------------------------------------  Creator  --------------------------------------------- */

GameModel* GameModel::create(){
    GameModel* instance = new(std::nothrow) GameModel();
    return instance;
};

GameModel::GameModel(){
    this->_initVariableWithConfigData();

    _score = 0;
    _carsAmount = 0;
    _heartsAmount = 0;
    _health = GameModel::MAX_HEALTH;
    _carCreateTimer = 3;
    _animalCreateTimer = Tools::randomNumber<float>(GameModel::MIN_ANIMAL_CREATE_TIME, GameModel::MAX_ANIMAL_CREATE_TIME);
    _carCreateTimer = Tools::randomNumber<float>(GameModel::MIN_CAR_CREATE_TIME, GameModel::MAX_CAR_CREATE_TIME);
    _heartCreateTimer = Tools::randomNumber<float>(GameModel::MIN_HEART_CREATE_TIME, GameModel::MAX_HEART_CREATE_TIME);
    _availableAnimalTypesVector = {
        GameConfig::ANIMAL_TYPE.at("CAT")
    };
};

void GameModel::_initVariableWithConfigData(){
    REWARD_SCORE = {
        {GameConfig::ANIMAL_TYPE.at("CAT"), 1},
        {GameConfig::ANIMAL_TYPE.at("LION"), 3},
        {GameConfig::ANIMAL_TYPE.at("LEOPARD_CAT"), 5},
        {GameConfig::ANIMAL_TYPE.at("CHEETAH"), 10}
    };
    ANIMAL_AVAILABLE_SCORE = {
        {GameConfig::ANIMAL_TYPE.at("CAT"), 0},
        {GameConfig::ANIMAL_TYPE.at("LION"), 10},
        {GameConfig::ANIMAL_TYPE.at("LEOPARD_CAT"), 35},
        {GameConfig::ANIMAL_TYPE.at("CHEETAH"), 140},
    };
};

/* ---------------------------------------------  Public  --------------------------------------------- */

void GameModel::update(float dt){
    this->_updateCardCreateTimer(dt);
    this->_updateAnimalCreateTimer(dt);
    this->_updateHeartCreateTimer(dt);
};

void GameModel::addScore(AnimalType animalType){
    _score = _score + REWARD_SCORE.at(animalType);
    this->_checkNewAvailableAnimalAppear();
};

void GameModel::addHealth(){
    _health = _health + 1;
    if (_health > GameModel::MAX_HEALTH){
        _health = GameModel::MAX_HEALTH;
    };
};

void GameModel::minusHealth(){
    _health = _health - 1;
    if (_health <= 0){
        this->_dispatchEvent(GameModel::EVENT.at("OUT_OF_HEALTH"));
    };
};

void GameModel::minusCarsAmount(){
    _carsAmount = _carsAmount - 1;
    if (_carsAmount < 0){
        _carsAmount = 0;
    };
};

void GameModel::minusHeartsAmount(){
    _heartsAmount = _heartsAmount - 1;
    if (_heartsAmount < 0){
        _heartsAmount = 0;
    };
};

int GameModel::getScore(){
    return _score;
};

int GameModel::getHealth(){
    return _health;
};

void GameModel::setEventListener(GameModelEventListener eventListener){
    _eventListener = eventListener;
};

/* ---------------------------------------------  Private  --------------------------------------------- */

/* ------------------------------- */
/* ------- Update Function ------- */
/* ------------------------------- */

void GameModel::_updateCardCreateTimer(float dt){
    if (_carsAmount == GameModel::MAX_ON_ROAD_CAR_AMOUNT){
        return;
    };

    _carCreateTimer = _carCreateTimer - dt;
    if (_carCreateTimer <= 0){
        _carsAmount = _carsAmount + 1;
        _carCreateTimer = Tools::randomNumber<float>(GameModel::MIN_CAR_CREATE_TIME, GameModel::MAX_CAR_CREATE_TIME);
        this->_dispatchEvent(GameModel::EVENT.at("CREATE_CAR"));
    };
};

void GameModel::_updateAnimalCreateTimer(float dt){
    _animalCreateTimer = _animalCreateTimer - dt;
    if (_animalCreateTimer <= 0){
        _animalCreateTimer = Tools::randomNumber<float>(GameModel::MIN_ANIMAL_CREATE_TIME, GameModel::MAX_ANIMAL_CREATE_TIME);
        AnimalType animalType = Tools::getVectorRandomValue<AnimalType>(_availableAnimalTypesVector);
        this->_dispatchEvent(
            GameModel::EVENT.at("CREATE_ANIMAL"),
            static_cast<std::string*>(&animalType)
        );
    };
};

void GameModel::_updateHeartCreateTimer(float dt){
    if (_heartsAmount == GameModel::MAX_ON_ROAD_HEART_AMOUNT){
        return;
    };

    _heartCreateTimer = _heartCreateTimer - dt;
    if (_heartCreateTimer < 0){
        _heartsAmount = _heartsAmount + 1;
        _heartCreateTimer = Tools::randomNumber<float>(GameModel::MIN_HEART_CREATE_TIME, GameModel::MAX_HEART_CREATE_TIME);  
        this->_dispatchEvent(GameModel::EVENT.at("CREATE_HEART"));
    };
};

/* ------------------------------- */
/* -------- Other Function ------- */
/* ------------------------------- */

void GameModel::_dispatchEvent(GameModelEvent event, void* customData){
    if (_eventListener != nullptr){
        _eventListener(event, customData);
    };
};

void GameModel::_checkNewAvailableAnimalAppear(){
    std::vector<AnimalType> availableAnimalTypesVector = this->_getAvailableCreateAnimalTypesVector(_score);
    std::vector<AnimalType> newAnimalTypesVector = Tools::getVectorsDiff<AnimalType>(
        availableAnimalTypesVector,
        _availableAnimalTypesVector
    );
    if (newAnimalTypesVector.empty()){
        return; // 沒有新動物出現
    };

    _availableAnimalTypesVector = availableAnimalTypesVector;
    for (int index = 0; index < newAnimalTypesVector.size(); index++){
        CCLOG("newAnimalTypesVector.at(index) %s", newAnimalTypesVector.at(index).c_str());
        this->_dispatchEvent(
            GameModel::EVENT.at("NEW_ANIMAL_APPEAR"),
            static_cast<std::string*>(&newAnimalTypesVector.at(index))
        );
    };
};

std::vector<AnimalType> GameModel::_getAvailableCreateAnimalTypesVector(int score){
    std::vector<AnimalType> availableAnimalTypes = {};
    for (auto availableAnimalData : ANIMAL_AVAILABLE_SCORE){
        AnimalType availableAnimalType = availableAnimalData.first;
        int availableScore = availableAnimalData.second;
        if (score >= availableScore){
            availableAnimalTypes.push_back(availableAnimalType);
        };
    };
    return availableAnimalTypes;
};