#include "MainScene.h"
#include "Lobby/LobbyLayer.h"
#include "Game/GameLayer.h"
#include "Common/Layer/LayerSwitcher.h"
#include "Common/View/AudioButton.h"
#include "Common/View/SwitchButton.h"
#include "Common/Config.h"
#include "editor-support/cocostudio/SimpleAudioEngine.h"
#include "Common/View/Button.h"

USING_NS_CC;

bool MainScene::init(){
    this->_loadPlistImages();
    AudioButton::switchOnFilePath = "btn/btn_sound_on.png";
    AudioButton::switchOffFilePath = "btn/btn_sound_off.png";    

    _layerSwitcher = this->_initLayerSwitcher();
    _layerSwitcher->switchLayer(Config::LOBBY_NAME);
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("audio/bgm.mp3", true);

    return true;
};

void MainScene::_loadPlistImages(){
    auto textureCache = Director::getInstance()->getTextureCache();
    auto spriteFrameCache =  SpriteFrameCache::getInstance();

    textureCache->addImage("animal/animal.png");
    spriteFrameCache->addSpriteFramesWithFile("animal/animal.plist");

    textureCache->addImage("car/car.png");
    spriteFrameCache->addSpriteFramesWithFile("car/car.plist");
};

LayerSwitcher* MainScene::_initLayerSwitcher(){
    LobbyLayer* lobbyLayer = LobbyLayer::create();
    GameLayer* gameLayer = GameLayer::create();

    LayerSwitcher* layerSwitcher = LayerSwitcher::getInstance();
    layerSwitcher->registerTargetScene(this);
    layerSwitcher->addLayer(Config::LOBBY_NAME, lobbyLayer);
    layerSwitcher->addLayer(Config::GAME_NAME, gameLayer);

    return layerSwitcher;
};