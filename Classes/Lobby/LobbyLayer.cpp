#include "Lobby/LobbyLayer.h"
#include "Common/Config.h"
#include "Common/Tools.h"
#include "Common/Layer/LayerSwitcher.h"
#include "Common/View/Button.h"
#include "Game/Game/View/GameView.h"

USING_NS_CC;

const std::string LobbyLayer::LOGO_FILE_PATH              = "logo/logo.png";
const std::string LobbyLayer::BTN_START_NORMAL_FILE_PATH  = "btn/btn_play.png";
const std::string LobbyLayer::BTN_START_PRESSED_FILE_PATH = "btn/btn_play.png";
const float LobbyLayer::MAX_CAR_CREATE_TIME               = 7;
const float LobbyLayer::MIN_CAR_CREATE_TIME               = 5;

// ----------------------------------------------  Creator  ----------------------------------------------

bool LobbyLayer::init(){
    return true;
};

void LobbyLayer::createLayer(){
    _carCreateTime = Tools::randomNumber<float>(LobbyLayer::MIN_CAR_CREATE_TIME, LobbyLayer::MAX_CAR_CREATE_TIME);

    _gameView      = this->_createGameView();
    _logo          = this->_createLogo();
    _startButton   = this->_createStartButton();
    this->scheduleUpdate();
};

void LobbyLayer::removeLayer(){
    this->removeAllChildren();
};

// ----------------------------------------------  Public  ----------------------------------------------

void LobbyLayer::update(float dt){
    _carCreateTime = _carCreateTime - dt;
    if (_carCreateTime <= 0){
        _carCreateTime = Tools::randomNumber<float>(LobbyLayer::MIN_CAR_CREATE_TIME, LobbyLayer::MAX_CAR_CREATE_TIME);
        _gameView->createRandomCar();
    };

    _gameView->update(dt);
};

// ----------------------------------------------  Private  ----------------------------------------------

GameView* LobbyLayer::_createGameView(){
    GameView* gameView = GameView::create();
    this->addChild(gameView);
    return gameView;
};

Sprite* LobbyLayer::_createLogo(){
    const Vec2 LOGO_POS = Vec2(Config::WIN_SIZE.width / 2, Config::WIN_SIZE.height / 2 + 100);

    Sprite* logo = Sprite::create(LobbyLayer::LOGO_FILE_PATH);
    logo->setPosition(Vec2(LOGO_POS));
    this->addChild(logo);
    return logo;
};

Button* LobbyLayer::_createStartButton(){
    const Vec2 BTN_START_POS = Vec2(Config::WIN_SIZE.width / 2, Config::WIN_SIZE.height / 2 - 150);

    Button* startButton = Button::create(
        LobbyLayer::BTN_START_NORMAL_FILE_PATH,
        LobbyLayer::BTN_START_PRESSED_FILE_PATH,
        [this](Ref* sender){
            this->unscheduleUpdate();
            LayerSwitcher::getInstance()->switchLayer(Config::GAME_NAME);
        }
    );
    startButton->setPosition(BTN_START_POS);
    this->addChild(startButton);

    return startButton;
};

