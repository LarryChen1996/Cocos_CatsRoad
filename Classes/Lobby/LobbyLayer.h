#ifndef __LOBBYLAYER_H__
#define __LOBBYLAYER_H__

#include "cocos2d.h"
#include "Common/Layer/BaseLayer.h"
#include "Common/View/Button.h"
#include "Game/Game/View/GameView.h"

class LobbyLayer : public BaseLayer{
    public:
        // creator
        CREATE_FUNC(LobbyLayer);
        virtual bool init() override;
        virtual void createLayer() override;
        virtual void removeLayer() override;

        // function
        virtual void update(float dt) override;

    private: 
        // class const variable
        const static std::string LOGO_FILE_PATH;
        const static std::string BTN_START_NORMAL_FILE_PATH;
        const static std::string BTN_START_PRESSED_FILE_PATH;
        const static float MAX_CAR_CREATE_TIME;
        const static float MIN_CAR_CREATE_TIME;

        // variable
        float _carCreateTime;
        GameView* _gameView;
        cocos2d::Sprite* _logo;
        Button* _startButton;
        
        // function
        void _doStartButtonCallback();
        cocos2d::Sprite* _createLogo();
        Button* _createStartButton();
        GameView* _createGameView();

        /* 範例 請勿刪除
        std::function<void()> _startButtonCallback;
        */
};

#endif // __LOBBYLAYER_H__